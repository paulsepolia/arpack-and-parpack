#!/bin/bash

# 1. compile

  ifort  -e08                     \
         -warn all                \
	     -O3                      \
         -static                  \
         -assume buffered_io      \
         m_1_type_definitions.f90 \
         driver_coo_direct.f90    \
         -o x_intel

# 2. clean

  rm *.mod
