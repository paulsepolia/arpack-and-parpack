!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2014/12/11              !
!===============================!

program driver_arp_dense_sym_half_disk

  use m_1_type_definitions
  use m_2_matrix_dense_sym_half_to_ram
  use m_4_arp_dense_sym_half

  implicit none

! 1. driver/input variables

  real(kind=dr), allocatable, dimension(:)   :: matrix
  integer(kind=si)                           :: matrix_dimen
  character(len=200)                         :: matrix_file_name
  character(len=200)                         :: type_of_format_for_matrix
  real(kind=dr), allocatable, dimension(:,:) :: vectors_array
  real(kind=dr), allocatable, dimension(:,:) :: values_array
  integer(kind=si)                           :: nev
  integer(kind=si)                           :: maxitr
  real(kind=dr)                              :: tol
  character(len=2)                           :: which
  integer(kind=si)                           :: ncv_coeff

! 2. local variables for outputting the produced eigensystem

  character(len=200) :: type_of_format_for_output
  character(len=200) :: file_for_eigenvalues
  character(len=200) :: file_for_eigenvectors
 
! 3. local variables and parameters
  
  real(kind=dr), parameter :: zero = 0.0_dr
  integer(kind=di) :: i1, i2
  integer(kind=si) :: URW = 11

!========================================================================================!
!========================================================================================!
! 4. all the necessary input for the driver

  ! 4-1. name of the matrix file in hard disk

  matrix_file_name = "matrix_dense_sym_half"

  ! 4-2. type of format of the matrix file

  type_of_format_for_matrix = "unformatted"

  ! 4-3. type of format of the output eigensystem

  type_of_format_for_output = "formatted"

  ! 4-4. name of the file for eigenvalues

  file_for_eigenvalues = "___va"

  ! 4-5. name of the file for eigenvectors

  file_for_eigenvectors = "___ve"

  ! 4-6. number of eigenvalues/eigenvectors to get back

  nev = 200

  ! 4-7. maximum arnoldi restarts

  maxitr = 3000000

  ! 4-8. the convergence criterion

  tol = zero
 
  ! 4-9. the part of the spectrum

  which = 'SA'

  ! 4-10. the part 'ncv_coeff'

  ncv_coeff = 2

!========================================================================================!
!========================================================================================!

! 5. calling the subroutine which reads form the hard disk the matrix: 'matrix_file_name'
!    and writes in to the ram the variables scalar/arrays:

  call matrix_dense_sym_half_to_ram(matrix_file_name,          &
                                    type_of_format_for_matrix, &
                                    matrix_dimen,              &
                                    matrix)

! 6. testing. outputting some characteristics of the matrix: "matrix_file_name".

  write(*,*) "==========================================================================="
  write(*,*) "==========================================================================="
  write(*,*) " Inside driver program"
  write(*,*) 
  write(*,*) " Some characteristics values of the matrix under diagonalization"
  write(*,*) 
  write(*,*) " matrix_dimen --> ", matrix_dimen
  write(*,*) " matrix(1)    --> ", matrix(1)
  write(*,*) " matrix(2)    --> ", matrix(2)
  write(*,*) " matrix(3)    --> ", matrix(3)
  write(*,*) " matrix(4)    --> ", matrix(4)
  write(*,*) "==========================================================================="

! 7. calling the subroutine which diagonalizes the matrix: "matrix_file_name"

  call arpack_dense_sym_half(matrix,        &  !  1. input
                             matrix_dimen,  &  !  2. input
                             which,         &  !  3. input
                             ncv_coeff,     &  !  4. input
                             nev,           &  !  5. input
                             maxitr,        &  !  6. input
                             tol,           &  !  7. input
                             vectors_array, &  !  8. output
                             values_array)     !  9. output

  
! 10. writing to the hard disk the eigensystem

  if (type_of_format_for_output == "unformatted") then

    ! 1. eigenvalues area. unformatted output

    open(unit=URW,                      &
         file=file_for_eigenvalues,     &
         status="new",                  &
         action="write",                &
         form=type_of_format_for_output)

    do i1 = 1, nev

      write(unit=URW) values_array(i1,1)

    end do

    flush(unit=URW)
  
    close(unit=URW, status="keep")

    ! 2. eigenvectors area. unformatted output

    open(unit=URW,                      &
         file=file_for_eigenvectors,    &
         status="new",                  &
         action="write",                &
         form=type_of_format_for_output)


    do i1 = 1, nev
      do i2 = 1, matrix_dimen

        write(unit=URW) vectors_array(i2,i1)

      end do
    end do

    flush(unit=URW)

    close(unit=URW, status="keep")

  else if (type_of_format_for_output == "formatted") then

    ! 3. eigenvalues area. formatmatted output

    open(unit=URW,                      &
         file=file_for_eigenvalues,     &
         status="new",                  &
         action="write",                &
         form=type_of_format_for_output)

    do i1 = 1, nev

      write(unit=URW,fmt=*) values_array(i1,1)

    end do

    flush(unit=URW)

    close(unit=URW, status="keep")

    ! 4. eigenvectors area. formatted output

    open(unit=URW,                      &
         file=file_for_eigenvectors,    &
         status="new",                  &
         action="write",                &
         form=type_of_format_for_output)


    do i1 = 1, nev
      do i2 = 1, matrix_dimen

        write(unit=URW,fmt=*) vectors_array(i2,i1)

      end do
    end do

    flush(unit=URW)

    close(unit=URW, status="keep")

  else 

    write(*,*) " error: type of format must be 'formatted' of 'unformatted' "
    stop   

  end if

! 10. deallocations

  deallocate(matrix)
  deallocate(values_array)
  deallocate(vectors_array)

end program driver_arp_dense_sym_half_disk

! FINI
