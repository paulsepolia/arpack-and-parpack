#!/bin/bash

# 1. compiling the code

  gfortran  -O3                      \
		    -std=f2008               \
		    -static                  \
            -fopenmp                 \
            build_dense_sym_half.f90 \
            -o x_gnu

# 2. exiting
