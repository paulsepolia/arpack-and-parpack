
  program build_dense_sym_half
  
  implicit none


  ! 1. local variables and parameters

  integer, parameter :: dr = selected_real_kind(p=15,r=300)
  integer, parameter :: sr = selected_real_kind(p=6,r=30)
  integer, parameter :: di = selected_int_kind(18)
  integer, parameter :: si = selected_int_kind(9)
  real(kind=dr), allocatable, dimension(:) :: matrix_dense_half
  integer(kind=si)                         :: matrix_dimen
  integer(kind=si)                         :: i1, i2
  character(len=200)                       :: matrix_file_name
  integer(kind=di)                         :: indx
  integer(kind=si), parameter              :: URW = 12

  ! 2. giving input to the program

  matrix_dimen = 5000
  matrix_file_name = "matrix_dense_sym_half"
  indx = 0

  ! 3. allocating ram space

  allocate(matrix_dense_half(1:(matrix_dimen**2 + matrix_dimen)/2))

  matrix_dense_half = 0.0d0

  ! 4. building matrix in to ram
    
  do i1 = 1, matrix_dimen
    do i2 = i1, matrix_dimen
 
      indx = indx + 1

      if (i1 == i2) then
        matrix_dense_half(indx) = 2.5d0 * (i1+1.0d0) * (i2+1.0d0)
      else if (i1 /= i2) then  
        matrix_dense_half(indx) = (i1+1.0d0) * (i2+1.0d0) / 3.0d0
      end if

    end do
  end do
   
  ! 5. open unit to write to the disk

  open(unit=URW,              &
       file=matrix_file_name, &
       form="unformatted",    &
       action="write",        &
       status="new")      

  ! 6. writing to hard disk
  
  ! a.

  write(unit=URW) matrix_dimen 

  ! b.

  indx = 0 
  
  do i1 = 1, matrix_dimen
    do i2 = i1, matrix_dimen
   
      indx = indx + 1  
 
      write(unit=URW)  matrix_dense_half(indx)
 
    end do
  end do
 
  ! 7. closing the unit

  flush(unit=URW)

  close(unit=URW, status="keep")

  end program build_dense_sym_half

! FINI
