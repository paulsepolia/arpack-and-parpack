!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2014/12/11              !
!===============================!

  module m_3_mat_dense_half_vec_product

  use m_1_type_definitions

  implicit none

  contains

!==============================================================================!
!==============================================================================!
!
! 1.
!
! 1a. Subroutine: mv_dense_sym_half(matrix,       &
!                                   matrix_dimen, & 
!                                   x,            &
!                                   y)
!
! 1b. This subroutine is an impementation of a 
!     dense-symmetric-matrix-vector product.
!     The dense matrix is stored as a vector 
!     which contains only the half elements
!     (lower triangle or upper triangle).
!     The purpose of this impementation is to save RAM space
!     and also to speed up the dense-matrix-vector product.
!
! 1c. The following code developed using Mathematica. 
!     Strictly speaking is a copy-paste Mathematica code.
!     
!==============================================================================!
!==============================================================================!

  subroutine mat_dense_half_vec_product(matrix,       & 
                                        matrix_dimen, &
                                        x,            &
                                        y)

  implicit none

  ! 1. input variables

  real(kind=dr),    intent(in),  allocatable, dimension(:) :: matrix
  integer(kind=si), intent(in)                             :: matrix_dimen
  real(kind=dr),    intent(in),  allocatable, dimension(:) :: x
  real(kind=dr),    intent(out), allocatable, dimension(:) :: y

  ! 2. local variables

  integer(kind=di) :: i1, i2, i3
  integer(kind=di) :: dim_total
  integer(kind=si), allocatable, dimension(:) :: peakF
  integer(kind=si), allocatable, dimension(:) :: downF
  real(kind=dr)   , allocatable, dimension(:) :: mat_vec
  integer(kind=si) :: i1_help
  integer(kind=si) :: i1_do, i2_do, i3_do
  integer(kind=si) :: i
  real(kind=dr) :: k_tmp

  ! 3.

  allocate(mat_vec(1:matrix_dimen))
  allocate(peakF(1:matrix_dimen))
  allocate(downF(1:matrix_dimen))
  allocate(y(1:matrix_dimen))

  ! 4.

  !$omp parallel             &
  !$omp default(none)        &
  !$omp shared(y)            &
  !$omp private(i)           &
  !$omp shared(matrix_dimen)

  !$omp do

  do i = 1, matrix_dimen

    y(1:matrix_dimen) = 0.0d0  ! parallel
  
  end do

  !$omp end do

  !$omp end parallel

  ! a.

  i1 = 0_di

  do i1_do = 1, matrix_dimen

    i1 = i1 + 1_di 
    i1_help = ((-i1+3)*i1)/2 + matrix_dimen*(i1-1)  
    peakF(i1_do) = i1_help 

  end do

  ! b.

  i1 = 0_di

  do i1_do = 1, matrix_dimen
  
    i1 = i1 + 1_di
    i1_help = ((1-i1)*i1)/2 + matrix_dimen*(i1) 
    downF(i1_do) = i1_help   

  end do

  ! c.

  i1 = 0_di

  do i1_do = 1, matrix_dimen

    ! c1.

    i1 = i1 + 1_di
    mat_vec(1) = matrix(i1)

    ! c2.

    !$omp parallel             &
    !$omp default(none)        &
    !$omp shared(mat_vec)      &
    !$omp shared(matrix)       &
    !$omp shared(matrix_dimen) &
    !$omp shared(i1_do)        &
    !$omp private(i3_do)

    !$omp do

    do i3_do = 1, i1_do-2 ! parallel

      mat_vec(i3_do+1) = matrix(i1_do+(-i3_do+2*matrix_dimen*i3_do-i3_do**2)/2)

    end do

    !$omp end do

    !$omp end parallel

    ! c3.

    !$omp parallel             &
    !$omp default(none)        &
    !$omp shared(matrix)       &
    !$omp shared(mat_vec)      &
    !$omp shared(peakF)        &
    !$omp shared(downF)        &
    !$omp private(i)           &
    !$omp shared(i1_do)        &
    !$omp shared(matrix_dimen)

    !$omp do 

    do i = i1_do, matrix_dimen

      mat_vec(i) = matrix(peakF(i1_do)+i-i1_do)

    end do

    !$omp end do
 
    !$omp end parallel

    ! c4.

    k_tmp = y(i1_do)

    !$omp parallel             &
    !$omp default(none)        &
    !$omp shared(matrix_dimen) &
    !$omp shared(mat_vec)      &
    !$omp shared(x)            &
    !$omp private(i2_do)       &
    !$omp shared(i1_do)        &
    !$omp reduction(+:k_tmp)

    !$omp do

    do i2_do = 1, matrix_dimen

      k_tmp = k_tmp + mat_vec(i2_do) * x(i2_do)

    end do

    !$omp end do

    !$omp end parallel

    y(i1_do) = k_tmp

  end do

  end subroutine mat_dense_half_vec_product

  end module m_3_mat_dense_half_vec_product

! FINI
