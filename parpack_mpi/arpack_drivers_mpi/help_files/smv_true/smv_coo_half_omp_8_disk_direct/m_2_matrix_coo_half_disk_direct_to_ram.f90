!====================================================================!
!                                                                    !
! author: pavlos g. galiatsatos.                                     !
!                                                                    ! 
! date: 2010/11/16.                                                  !
!                                                                    !
!====================================================================!

module m_2_matrix_coo_half_disk_direct_to_ram

  use m_1_type_definitions

  implicit none

  contains
  
!===================================================================================================! --> 1.
!===================================================================================================!

  subroutine matrix_coo_half_disk_direct_to_ram( matrix_file,              &
                                                 type_of_format,           & 
                                                 matrix_dimen,             &
                                                 non_zero_ham_elem_half,   &
                                                 row_index_array,          &            
                                                 column_index_array,       &
                                                 ham_elem_array )    
                                    

  implicit none

  ! 1.

  character(len=200) :: matrix_file
  character(len=200) :: type_of_format

  integer(kind=si), dimension(:), allocatable, intent(out) :: row_index_array
  integer(kind=si), dimension(:), allocatable, intent(out) :: column_index_array
  real(kind=dr)   , dimension(:), allocatable, intent(out) :: ham_elem_array
 
  integer(kind=si), intent(out) :: matrix_dimen
  integer(kind=dr) :: matrix_dimen_tmp
  integer(kind=di), intent(out) :: non_zero_ham_elem_half
  integer(kind=dr) :: non_zero_ham_elem_half_tmp
  real(kind=dr):: co_tmp, ro_tmp
  integer(kind=si) :: rec_len
  real(kind=dr), dimension(3) :: dd 

  ! 2. local variables

  integer(kind=di) :: i1
 
  ! 3.
  inquire( iolength = rec_len ) dd

  open( unit = 11,             &
        form = type_of_format, &
        file = matrix_file,    &
        access = "direct",     &
        status = "old",        &
        action = "read",       &
        recl = rec_len )  

  ! 4.

  if ( type_of_format == "unformatted" ) then 

    read( unit=11, rec = 1 ) dd(1), dd(2), dd(3) 

    non_zero_ham_elem_half = dd(3)
    matrix_dimen = dd(1)

  else 

    write(*,*)  " error in the chosen format: ", type_of_format
    write(*,*)  " the chosen format must be 'formatted' or 'unformatted'." 
    stop
 
  end if

  close ( unit = 11, status = "keep" )

  ! 5.

  allocate( row_index_array     (1:non_zero_ham_elem_half) )
  allocate( column_index_array  (1:non_zero_ham_elem_half) )
  allocate( ham_elem_array      (1:non_zero_ham_elem_half) )
  
  ! 6.
  
  open( unit = 11,             &
        form = type_of_format, &
        file = matrix_file,    &
        access = "direct",     &
        status = "old",        &
        action = "read",       &
        recl = rec_len )  

  if ( type_of_format == "unformatted" ) then
  
    do i1 = 1 , non_zero_ham_elem_half

      read( unit=11, rec=i1+1 ) dd(1), dd(2), dd(3) 

      column_index_array(i1)    = dd(1)
      row_index_array(i1)       = dd(2)
      ham_elem_array(i1)        = dd(3)

    end do 

  end if

  ! 7. 
 
  close( unit=11,  status="keep" )

  end subroutine matrix_coo_half_disk_direct_to_ram
 
!===================================================================================================! --> 2.
!===================================================================================================!

end module m_2_matrix_coo_half_disk_direct_to_ram
