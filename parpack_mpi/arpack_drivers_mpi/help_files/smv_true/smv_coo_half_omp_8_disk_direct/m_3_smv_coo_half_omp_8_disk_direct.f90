!====================================================================!
!                                                                    !
! Author: Pavlos G. Galiatsatos.                                     !
!                                                                    !
! Date: 2010/11/16.                                                  !
!                                                                    !
!====================================================================!

module m_3_smv_coo_half_omp_8_disk_direct

  use m_1_type_definitions
  use m_2_matrix_coo_half_disk_direct_to_ram

  implicit none

  contains

  subroutine smv_coo_half_omp_8(  row_index_array,      &  
                                  column_index_array,   &  
                                  ham_elem_array,       &  
                                  matrix_dimen,         &     
                                  non_zero_ham_elem,    &
                                  x,                    &
                                  y )     
              

  implicit none

! 1. variables.

  integer(kind=si), intent(in),  allocatable, dimension(:) :: row_index_array        
  integer(kind=si), intent(in),  allocatable, dimension(:) :: column_index_array     
  real(kind=dr),    intent(in),  allocatable, dimension(:) :: ham_elem_array         
  real(kind=dr),    intent(in),  allocatable, dimension(:) :: x
  real(kind=dr),    intent(out), allocatable, dimension(:) :: y  
  integer(kind=si), intent(inout)                          :: matrix_dimen                                  
  integer(kind=di), intent(inout)                          :: non_zero_ham_elem                             

! 2. openmp help variables. openmp part 1.

  integer(kind=si), parameter :: one = 1
  integer(kind=si), parameter :: openmp_threads = 8 
  
  real(kind=dr), allocatable, dimension(:) :: x1   ! x1
  real(kind=dr), allocatable, dimension(:) :: x2   ! x2
  real(kind=dr), allocatable, dimension(:) :: x3   ! x3
  real(kind=dr), allocatable, dimension(:) :: x4   ! x4
  real(kind=dr), allocatable, dimension(:) :: x5   ! x5
  real(kind=dr), allocatable, dimension(:) :: x6   ! x6
  real(kind=dr), allocatable, dimension(:) :: x7   ! x7
  real(kind=dr), allocatable, dimension(:) :: x8   ! x8

  real(kind=dr), allocatable, dimension(:) :: y1   ! y1
  real(kind=dr), allocatable, dimension(:) :: y2   ! y2
  real(kind=dr), allocatable, dimension(:) :: y3   ! y3
  real(kind=dr), allocatable, dimension(:) :: y4   ! y4
  real(kind=dr), allocatable, dimension(:) :: y5   ! y5
  real(kind=dr), allocatable, dimension(:) :: y6   ! y6
  real(kind=dr), allocatable, dimension(:) :: y7   ! y7
  real(kind=dr), allocatable, dimension(:) :: y8   ! y8
  
  integer(kind=di) :: i1
  integer(kind=si) :: irow, icolumn

! 3. allocating ram space.

  allocate(y(1:matrix_dimen))  ! only this - intent(out)
 
  allocate(x1(1:matrix_dimen))   ! x1
  allocate(x2(1:matrix_dimen))   ! x2
  allocate(x3(1:matrix_dimen))   ! x3
  allocate(x4(1:matrix_dimen))   ! x4
  allocate(x5(1:matrix_dimen))   ! x5
  allocate(x6(1:matrix_dimen))   ! x6
  allocate(x7(1:matrix_dimen))   ! x7
  allocate(x8(1:matrix_dimen))   ! x8

  allocate(y1(1:matrix_dimen))   ! y1
  allocate(y2(1:matrix_dimen))   ! y2
  allocate(y3(1:matrix_dimen))   ! y3
  allocate(y4(1:matrix_dimen))   ! y4
  allocate(y5(1:matrix_dimen))   ! y5
  allocate(y6(1:matrix_dimen))   ! y6
  allocate(y7(1:matrix_dimen))   ! y7
  allocate(y8(1:matrix_dimen))   ! y8

! 4. initializing openmp help variables.

  x1 = x    ! x1
  x2 = x    ! x2
  x3 = x    ! x3
  x4 = x    ! x4
  x5 = x    ! x5
  x6 = x    ! x6
  x7 = x    ! x7
  x8 = x    ! x8

  y1 = 0d0  ! y1
  y2 = 0d0  ! y2
  y3 = 0d0  ! y3
  y4 = 0d0  ! y4
  y5 = 0d0  ! y5
  y6 = 0d0  ! y6
  y7 = 0d0  ! y7
  y8 = 0d0  ! y8

! 5. openmp part.

!$OMP  PARALLEL                      &
!$OMP  DEFAULT( NONE )               &
!$OMP  SHARED( row_index_array )     &
!$OMP  SHARED( column_index_array )  &
!$OMP  SHARED( ham_elem_array )      &
!$OMP  SHARED( non_zero_ham_elem )   &
!$OMP  PRIVATE( i1, irow, icolumn )  &
!$OMP SHARED( x1 )                   &  !  x1   
!$OMP SHARED( x2 )                   &  !  x2   
!$OMP SHARED( x3 )                   &  !  x3   
!$OMP SHARED( x4 )                   &  !  x4   
!$OMP SHARED( x5 )                   &  !  x5   
!$OMP SHARED( x6 )                   &  !  x6   
!$OMP SHARED( x7 )                   &  !  x7   
!$OMP SHARED( x8 )                   &  !  x8   
!$OMP SHARED( y1 )                   &  !  y1   
!$OMP SHARED( y2 )                   &  !  y2   
!$OMP SHARED( y3 )                   &  !  y3   
!$OMP SHARED( y4 )                   &  !  y4   
!$OMP SHARED( y5 )                   &  !  y5   
!$OMP SHARED( y6 )                   &  !  y6   
!$OMP SHARED( y7 )                   &  !  y7   
!$OMP SHARED( y8 )                      !  y8   

!$OMP SECTIONS
!$OMP SECTION                           !  1
  
  do i1 = (0*non_zero_ham_elem)/openmp_threads+one, (1*non_zero_ham_elem)/openmp_threads
  
    irow = row_index_array(i1)
  
    icolumn = column_index_array(i1)
  
    y1(irow) = y1(irow) + ham_elem_array(i1) * x1(icolumn)
  
    if (irow.ne.icolumn) y1(icolumn) = y1(icolumn) + ham_elem_array(i1) * x1(irow)
  
  end do 

!$OMP SECTION                           !  2
  
  do i1 = (1*non_zero_ham_elem)/openmp_threads+one, (2*non_zero_ham_elem)/openmp_threads
  
    irow = row_index_array(i1)
  
    icolumn = column_index_array(i1)
  
    y2(irow) = y2(irow) + ham_elem_array(i1) * x2(icolumn)
  
    if (irow.ne.icolumn) y2(icolumn) = y2(icolumn) + ham_elem_array(i1) * x2(irow)
  
  end do 


!$OMP SECTION                           !  3
  
  do i1 = (2*non_zero_ham_elem)/openmp_threads+one, (3*non_zero_ham_elem)/openmp_threads
  
    irow = row_index_array(i1)
  
    icolumn = column_index_array(i1)
  
    y3(irow) = y3(irow) + ham_elem_array(i1) * x3(icolumn)
  
    if (irow.ne.icolumn) y3(icolumn) = y3(icolumn) + ham_elem_array(i1) * x3(irow)
  
  end do 

!$OMP SECTION                           !  4
  
  do i1 = (3*non_zero_ham_elem)/openmp_threads+one, (4*non_zero_ham_elem)/openmp_threads
  
    irow = row_index_array(i1)
  
    icolumn = column_index_array(i1)
  
    y4(irow) = y4(irow) + ham_elem_array(i1) * x4(icolumn)
  
    if (irow.ne.icolumn) y4(icolumn) = y4(icolumn) + ham_elem_array(i1) * x4(irow)
  
  end do 

!$OMP SECTION                           !  5
  
  do i1 = (4*non_zero_ham_elem)/openmp_threads+one, (5*non_zero_ham_elem)/openmp_threads
  
    irow = row_index_array(i1)
  
    icolumn = column_index_array(i1)
  
    y5(irow) = y5(irow) + ham_elem_array(i1) * x5(icolumn)
  
    if (irow.ne.icolumn) y5(icolumn) = y5(icolumn) + ham_elem_array(i1) * x5(irow)
  
  end do

!$OMP SECTION                           !  6
  
  do i1 = (5*non_zero_ham_elem)/openmp_threads+one, (6*non_zero_ham_elem)/openmp_threads
  
    irow = row_index_array(i1)
  
    icolumn = column_index_array(i1)
  
    y6(irow) = y6(irow) + ham_elem_array(i1) * x6(icolumn)
  
    if (irow.ne.icolumn) y6(icolumn) = y6(icolumn) + ham_elem_array(i1) * x6(irow)
  
  end do 

!$OMP SECTION                           !  7
  
  do i1 = (6*non_zero_ham_elem)/openmp_threads+one, (7*non_zero_ham_elem)/openmp_threads
  
    irow = row_index_array(i1)
  
    icolumn = column_index_array(i1)
  
    y7(irow) = y7(irow) + ham_elem_array(i1) * x7(icolumn)
  
    if (irow.ne.icolumn) y7(icolumn) = y7(icolumn) + ham_elem_array(i1) * x7(irow)
  
  end do 

!$OMP SECTION                           !  8
  
  do i1 = (7*non_zero_ham_elem)/openmp_threads+one, (8*non_zero_ham_elem)/openmp_threads
  
    irow = row_index_array(i1)
  
    icolumn = column_index_array(i1)
  
    y8(irow) = y8(irow) + ham_elem_array(i1) * x8(icolumn)
  
    if (irow.ne.icolumn) y8(icolumn) = y8(icolumn) + ham_elem_array(i1) * x8(irow)
  
  end do 

!$OMP END SECTIONS 
!$OMP END PARALLEL 

    y  =  & 
    y1 +  &   !  1.
    y2 +  &   !  2.
    y3 +  &   !  3.
    y4 +  &   !  4.
    y5 +  &   !  5.
    y6 +  &   !  6.
    y7 +  &   !  7.
    y8        !  8.

  end subroutine smv_coo_half_omp_8

end module m_3_smv_coo_half_omp_8_disk_direct
