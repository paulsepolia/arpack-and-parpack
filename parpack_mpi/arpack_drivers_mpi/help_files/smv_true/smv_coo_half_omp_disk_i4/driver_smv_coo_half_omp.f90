
program driver_smv_coo_half_omp

  use m_1_type_definitions
  use m_2_coo_half_to_ram
  use m_3_smv_coo_half_omp

  implicit none

! 1. interface.
  integer(kind=si),   parameter :: trials = 1000
  character(len=200), parameter :: matrix_file_name = "matrix_coo_half_i4"
  character(len=200), parameter :: type_of_format_for_matrix = "unformatted"

! 2. declarations.
  integer(kind=si)   :: matrix_dimen
  integer(kind=di)   :: non_zero_ham_elem
  integer(kind=si), allocatable, dimension(:) :: row_index_array
  integer(kind=si), allocatable, dimension(:) :: column_index_array
  real(kind=dr)   , allocatable, dimension(:) :: ham_elem_array 
  real(kind=dr)   , allocatable, dimension(:) :: x 
  real(kind=dr)   , allocatable, dimension(:) :: y 
  real(kind=dr) :: t1, t2  
  integer(kind=si) :: i1

! 3. calling the sub which reads the coo_half_sparse matrix.
  call coo_half_to_ram( matrix_file_name,           &
                        type_of_format_for_matrix,  &
                        matrix_dimen,               &
                        non_zero_ham_elem,          &
                        row_index_array,            &
                        column_index_array,         &
                        ham_elem_array )


  write(*,*) "==========================================================================="
  write(*,*) " Inside driver program."
  write(*,*) 
  write(*,*) " Some characteristics values of the matrix under diagonalization."
  write(*,*) 
  write(*,*) " matrix_dimen --> ", matrix_dimen
  write(*,*) " non_zero_ham_elem --> ", non_zero_ham_elem
  write(*,*) " row_index_array(100) --> ", row_index_array(100)
  write(*,*) " column_index_array(100) --> ", column_index_array(100)
  write(*,*) " ham_elem_array(100) --> ", ham_elem_array(100)
  write(*,*) 
  write(*,*) "==========================================================================="

! 4. initializing the x vector.
  allocate( x(1:matrix_dimen) ) ! only this - intent(in)

  do i1 = 1, matrix_dimen

    x(i1) = dcos(dcos( real(i1,kind=dr) ))

  end do

! 5. calling the sub sparse matrix vector product.
  call cpu_time(t1)

  do i1 = 1, trials

    call smv_coo_half_omp( row_index_array,      & 
                           column_index_array,   &
                           ham_elem_array,       &
                           matrix_dimen,         &
                           non_zero_ham_elem,    &
                           x,                    &
                           y )


  end do

  call cpu_time(t2)

! 6. writing out for testing purposes.
  write(*,*) " total cpu time for ", trials, " smv = ", (t2-t1)
  write(*,*) " cpu time per trial = ", (t2-t1) / trials

  do i1 = 1, matrix_dimen

    write(*,*) " x(", i1, ")=", x(i1)
    write(*,*) " y(", i1, ")=", y(i1)

  end do

! 7. deallocations.
  deallocate(x)
  deallocate(y)
  deallocate(row_index_array)
  deallocate(column_index_array)
  deallocate(ham_elem_array)

end program driver_smv_coo_half_omp
