!====================================================================!
!                                                                    !
! Author: Pavlos G. Galiatsatos.                                     !
!                                                                    ! 
! Date: 2011/07/16.                                                  !
!                                                                    !
!====================================================================!

module m_2_coo_half_to_ram

  use m_1_type_definitions

  implicit none

  contains
  
!==============================================================================! --> 1.
!==============================================================================!

  subroutine coo_half_to_ram( matrix_file,              &
                              type_of_format,           & 
                              matrix_dimen,             &
                              non_zero_ham_elem_half,   &
                              row_index_array,          &            
                              column_index_array,       &
                              ham_elem_array )    
                                    

  implicit none

  ! 1.

  character(len=200) :: matrix_file
  character(len=200) :: type_of_format

  integer(kind=si), dimension(:), allocatable, intent(out) :: row_index_array
  integer(kind=si), dimension(:), allocatable, intent(out) :: column_index_array
  real(kind=dr)   , dimension(:), allocatable, intent(out) :: ham_elem_array
 
  integer(kind=si), intent(out) :: matrix_dimen
  integer(kind=di), intent(out) :: non_zero_ham_elem_half

  ! 2. local variables

  integer(kind=di) :: i1
 
  ! 3.
  
  open( unit = 11,             &
        form = type_of_format, &
        file = matrix_file,    &
        status = "old",        &
        action = "read" )  

  ! 4.

  if ( type_of_format == "unformatted" ) then 

    read( unit=11 ) matrix_dimen, matrix_dimen, non_zero_ham_elem_half

  else if ( type_of_format == "formatted" ) then

    read( unit=11, fmt=* ) matrix_dimen, matrix_dimen, non_zero_ham_elem_half

  else 

    write(*,*)  " error in the chosen format: ", type_of_format
    write(*,*)  " the chosen format must be 'formatted' or 'unformatted'." 
    stop
 
  end if

  ! 5.

  allocate( row_index_array     (1:non_zero_ham_elem_half) )
  allocate( column_index_array  (1:non_zero_ham_elem_half) )
  allocate( ham_elem_array      (1:non_zero_ham_elem_half) )
  
  ! 6.
  
  if ( type_of_format == "unformatted" ) then
  
    do i1 = 1 , non_zero_ham_elem_half

      read( unit=11 ) row_index_array(i1),        &
                      column_index_array(i1),     &
                      ham_elem_array(i1)

    end do 
  
  else if ( type_of_format == "formatted" ) then

    do i1 = 1 , non_zero_ham_elem_half

      read( unit=11, fmt=* ) row_index_array(i1),        &
                             column_index_array(i1),     &
                             ham_elem_array(i1)

    end do

  end if

  ! 7. 
 
  close( unit=11,         &
         status="keep" )

  end subroutine coo_half_to_ram
 
!==============================================================================! --> 2.
!==============================================================================!

end module m_2_coo_half_to_ram
