
  program smv_coo_half_mpi_disk_direct
 
  use mpi

  implicit none

  ! 1. declaration of variables about type definitions

  integer, parameter :: dr = selected_real_kind(p=15,r=300)
  integer, parameter :: sr = selected_real_kind(p=6,r=30)
  integer, parameter :: di = selected_int_kind(18)
  integer, parameter :: si = selected_int_kind(9)
 
  ! 2.a. declaration of the rest variables

  integer(kind=si), parameter :: trials = 10

  ! 2.b. declarations of the rest valiables

  integer(kind=si) :: ierr
  integer(kind=si) :: my_rank
  integer(kind=si) :: p
  integer(kind=si) :: matrix_dimen
  integer(kind=di) :: non_zero_ham_elem_half
  integer(kind=si), allocatable, dimension(:) :: row_index_array
  integer(kind=si), allocatable, dimension(:) :: column_index_array
  real(kind=dr)   , allocatable, dimension(:) :: ham_elem_array
  integer(kind=di) :: non_zero_a
  integer(kind=di) :: mod_a
  integer(kind=di) :: i
  integer(kind=si) :: j
  integer(kind=si) :: irow, icolumn
  real(kind=dr), allocatable, dimension(:) :: x
  real(kind=dr), allocatable, dimension(:) :: y
  real(kind=dr), allocatable, dimension(:) :: y_final
  real(kind=sr)    :: t1_mpi, t2_mpi, t1, t2
  real(kind=dr), dimension(3) :: dimen_line
  integer(kind=si) :: rec_len

  ! 3. mpi start up

  call MPI_Init( ierr )

  ! 4. get my process rank

  call MPI_Comm_rank( MPI_COMM_WORLD, my_rank, ierr )

  ! 5. find out how many mpi processes are being used

  call MPI_Comm_size( MPI_COMM_WORLD, p, ierr )

  ! 6. opening the unit to read the size of the matrix
  !    and the number of the non-zero hamiltonian elements.

  inquire( iolength=rec_len ) dimen_line

  if ( my_rank == 0 ) then

    ! 6.a.
    open( unit = 11,                          &
          form = "unformatted",               &
          file = "matrix_coo_half_direct",    &
          status = "old",                     &
          action = "read",                    &
          access = "direct",                  &
          recl = rec_len )

    ! 6.b.
    read( unit=11, rec=1 ) dimen_line(1), dimen_line(2), dimen_line(3)
    
    matrix_dimen           = dimen_line(1)
    matrix_dimen           = dimen_line(2)
    non_zero_ham_elem_half = dimen_line(3)

    ! 6.c.
    close( unit=11, status="keep" )

  end if

  call MPI_Barrier( MPI_COMM_WORLD, ierr )

  ! 7. broadcasting the matrix dimension value
  !    and the number of non-zero hamiltonian elements

  call MPI_Bcast( matrix_dimen, 1, MPI_INTEGER, 0, MPI_COMM_WORLD, ierr )
  call MPI_Bcast( non_zero_ham_elem_half, 1, MPI_INTEGER8, 0, MPI_COMM_WORLD, ierr )
 
  call MPI_Barrier( MPI_COMM_WORLD, ierr )

  ! 8. finding the value of the dimension of the array
  !    that i have to allocate in each mpi thread

  if ( mod( non_zero_ham_elem_half, p ) == 0 ) then
    non_zero_a = non_zero_ham_elem_half / p
    mod_a = 0
  else
    non_zero_a = ( non_zero_ham_elem_half / p ) + 1
    mod_a = mod( non_zero_ham_elem_half, p )
  end if

  ! 9. allocating space for the local arrays
  !    to put the sparse matrix data in each mpi thread

  allocate( row_index_array(1:non_zero_a) )
  allocate( column_index_array(1:non_zero_a) )
  allocate( ham_elem_array(1:non_zero_a) )

  call MPI_Barrier( MPI_COMM_WORLD, ierr )

  ! 10. reading the sparse matrix data for rank 0

  if ( my_rank == 0 ) then

    ! 10.a.
    open( unit = 11,                          &
          form = "unformatted",               &
          file = "matrix_coo_half_direct",    &
          status = "old",                     &
          action = "read",                    & 
          access = "direct",                  &
          recl = rec_len )

    ! 10.b. 

    do i = 1, non_zero_a

      read( unit=11, rec = i+1 ) dimen_line(1), dimen_line(2), dimen_line(3)

      row_index_array(i)    = dimen_line(1)  
      column_index_array(i) = dimen_line(2) 
      ham_elem_array(i)     = dimen_line(3)

    end do

    ! 10.c.

    close( unit=11, status="keep" )

  end if

  call MPI_Barrier( MPI_COMM_WORLD, ierr )

  ! 11. reading the sparse matrix data
  !     from and for the rest of mpi threads
  !     which are less than the 'mod_a' variable

  if ( my_rank /= 0 .and. my_rank < mod_a ) then

    ! 11.a.

    open( unit = 11,                          &
          form = "unformatted",               &
          file = "matrix_coo_half_direct",    &
          status = "old",                     &
          action = "read",                    &
          access = "direct",                  &
          recl = rec_len )

    ! 11.b.

    do i = 1, non_zero_a

      read( unit=11, rec = 1 + i + non_zero_a * my_rank )  dimen_line(1), dimen_line(2), dimen_line(3)

      row_index_array(i)    = dimen_line(1)  
      column_index_array(i) = dimen_line(2) 
      ham_elem_array(i)     = dimen_line(3)

    end do

    ! 11.c.

    close( unit=11, status="keep" )
  
  end if

  call MPI_Barrier( MPI_COMM_WORLD, ierr )

  ! 12. reading the sparse matrix data
  !     from and for the rest of mpi threads
  !     which are greater or equal than 'mod_a' variable

  if ( my_rank /= 0 .and. my_rank >= mod_a ) then

    ! 12.a.

    open( unit = 11,                          &
          form = "unformatted",               &
          file = "matrix_coo_half_direct",    &
          status = "old",                     &
          action = "read",                    &
          access = "direct",                  &
          recl = rec_len )

    ! 12.b.

    do i = 1, non_zero_a - 1

      read( unit=11, rec = 1 + i + non_zero_a*mod_a + (my_rank-mod_a)*(non_zero_a-1) )  &
        dimen_line(1), dimen_line(2), dimen_line(3)

      row_index_array(i)    = dimen_line(1)  
      column_index_array(i) = dimen_line(2) 
      ham_elem_array(i)     = dimen_line(3)

    end do

    ! 12.c.

    row_index_array(non_zero_a)    = 1_di     ! arbitrary value
    column_index_array(non_zero_a) = 1_di     ! arbitrary value
    ham_elem_array(non_zero_a)     = 0.0_dr   ! set to zero - the only safe value

  end if

  call MPI_Barrier( MPI_COMM_WORLD, ierr )

  ! 13. the x vector - arbitrary for now

  allocate( x(1:matrix_dimen) )
  
  if ( my_rank == 0 ) then

    do j = 1, matrix_dimen
  
      x(j) = dcos(dcos( real(j,kind=dr) ))
 
    end do
 
  end if

  call MPI_Barrier( MPI_COMM_WORLD, ierr )

  ! 14. broadcasting the x vector to the rest of 

  call MPI_Bcast( x, matrix_dimen, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr )

  call MPI_Barrier( MPI_COMM_WORLD, ierr )

  ! 15. the mpi sparse matrix - vector product 
  ! 15.a.

  allocate( y(1:matrix_dimen) )

  ! 15.b.

  if ( my_rank == 0 ) then
    allocate( y_final(1:matrix_dimen) )
  end if

  call MPI_Barrier( MPI_COMM_WORLD, ierr )
  
  ! 15.c.

  if( my_rank == 0 ) then
    t1_mpi = MPI_Wtime()
    call cpu_time(t1)
  end if

  do j = 1, trials ! testing do loop

    y = 0.0_dr
 
    do i = 1, non_zero_a
  
      irow = row_index_array(i)
  
      icolumn = column_index_array(i)
  
      y(irow) = y(irow) + ham_elem_array(i) * x(icolumn)
  
      if (irow.ne.icolumn) y(icolumn) = y(icolumn) + ham_elem_array(i) * x(irow)
  
    end do 

    call MPI_Barrier( MPI_COMM_WORLD, ierr ) 

    ! 15.d.

    call MPI_Reduce( y, y_final, matrix_dimen,             &
                     MPI_DOUBLE_PRECISION,                 &   
                     MPI_SUM, 0, MPI_COMM_WORLD, ierr )

    call MPI_Barrier( MPI_COMM_WORLD, ierr )

  end do ! end of testing do loop

  if ( my_rank == 0 ) then
    t2_mpi = MPI_Wtime()
    call cpu_time(t2)
  end if


  ! 16. results.

  if ( my_rank == 0 ) then

    write(*,*) " "
    write(*,*) "  1. matrix_dimen = ", matrix_dimen, " my_rank = ", my_rank
    write(*,*) "  2. non zero elements half = ", non_zero_ham_elem_half, " my_rank = ", my_rank
    write(*,*) "  3. p = ", p
    write(*,*) "  4. non zero elements half / p = ", non_zero_ham_elem_half / p
    write(*,*) "  5. mod(non,p) = ", mod( non_zero_ham_elem_half, p )
    write(*,*) "  6. non_zero_a = ", non_zero_a
    write(*,*) "  7. total time mpi = ", t2_mpi-t1_mpi
    write(*,*) "  8. total time real = ", t2-t1 
    write(*,*) "  9. smv per second (mpi)  = ", trials/(t2_mpi-t1_mpi)
    write(*,*) " 10. smv per second (real) = ", trials/(t2-t1)

    do j = 1, matrix_dimen

!      write(*,*) " ========================================================= "
!      write(*,*) " from rank          ", my_rank
!      write(*,*) " row_index_array    ", i, row_index_array(i)
!      write(*,*) " column_index_array ", i, column_index_array(i)
!      write(*,*) " ham_elem_array     ", i, ham_elem_array(i)
!      write(*,*) " x(i)               ", i, x(i)
!      write(*,*) " y(i)               ", i, y(i)
!      write(*,*) " y_final(i)         ", i, y_final(i)


      write(*,*) " x(", j, ")=", x(j)
      write(*,*) " y(", j, ")=", y_final(j)


    end do

  end if

  ! 17. mpi shut down

  call MPI_Finalize( ierr )

  end program smv_coo_half_mpi_disk_direct
