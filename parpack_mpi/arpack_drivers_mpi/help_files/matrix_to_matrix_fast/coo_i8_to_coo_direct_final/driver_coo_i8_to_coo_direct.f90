
program driver_coo_i8_to_coo_direct

  use m_1_type_definitions
  use m_2_coo_i8_to_coo_direct

  implicit none

! 1. input to the subroutine.

  character(len=200) :: coo_matrix_i8
  character(len=200) :: coo_matrix_direct

!========================================================================================
! 2. the executable part begins here.
!========================================================================================

! 3. some typical names for the files.

  coo_matrix_i8 = "coo_matrix_i8"
  coo_matrix_direct = "coo_matrix_direct"

! 4. calling the subroutine.

  call coo_i8_to_coo_direct( coo_matrix_i8, coo_matrix_direct )

end program driver_coo_i8_to_coo_direct
