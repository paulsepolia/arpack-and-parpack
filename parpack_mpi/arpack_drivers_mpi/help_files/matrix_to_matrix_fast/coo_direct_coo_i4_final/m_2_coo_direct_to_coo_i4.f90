!====================================================================!
!                                                                    !
! author: pavlos g. galiatsatos.                                     !
!                                                                    !
! date: 2010/12/16.                                                  !
!                                                                    !
! infos: this module is a black-box module.                          !
!        you do not need to adjust anything.                         ! 
!====================================================================!

module m_2_coo_direct_to_coo_i4

  use m_1_type_definitions

  implicit none

  contains
  
!===================================================================================================! --> 1.
!===================================================================================================!
     
!=======================================================================================!
!                                                                                       !
! subroutine: "coo_direct_to_coo_i4".                                                   !
!                                                                                       !
! a. input:                                                                             !
!                                                                                       !
! character(len=200), intent(in) ::                                                     !
!                                                                                       !
! b. output:                                                                            !
!                                                                                       !
! c. purpose:                                                                           !
!                                                                                       !
!                                                                                       !
!=======================================================================================!

  subroutine coo_direct_to_coo_i4( coo_matrix_direct, coo_matrix_i4 )

  implicit none

! 1. inputs

  character(len=200), intent(in) :: coo_matrix_direct
  character(len=200), intent(in) :: coo_matrix_i4

! 2. local variables.

  integer(kind=si) :: dimen_matrix_i4
  real(kind=dr) :: dimen_matrix_direct
  real(kind=dr) :: non_zero_ham_elem_direct
  integer(kind=di) :: non_zero_ham_elem

  real(kind=dr), allocatable, dimension(:) :: row_index_array
  real(kind=dr), allocatable, dimension(:) :: column_index_array
  real(kind=dr), allocatable, dimension(:) :: ham_elem_array
  integer(kind=si), parameter :: unit_local = 11
  integer(kind=di) :: i1
  integer(kind=si) :: rec_len
  real(kind=dr), dimension(3) :: dimen_line

!====================================================================!
! 3. executable part begins here.
!====================================================================!

! 4. opening the "coo_matrix_direct" file.

  inquire( iolength=rec_len ) dimen_line ! evaluating the record length

  open( unit   = unit_local,             &
        file   = coo_matrix_direct,      &
        form   = "unformatted",          &
        status = "old",                  &
        action = "read",                 &
        access = "direct",               &
        recl   = rec_len ) 
 
! 5. reading the head of the "coo_matrix_direct" file.

  rewind(unit=unit_local)
  read(unit=unit_local, rec=1) dimen_matrix_direct,      &
                               dimen_matrix_direct,      &
                               non_zero_ham_elem_direct

  dimen_matrix_i4 = int( dimen_matrix_direct, kind=si )

! 6. allocating ram space.

  non_zero_ham_elem = int( non_zero_ham_elem_direct, kind=di )

  allocate( row_index_array(1:non_zero_ham_elem) ) 
  allocate( column_index_array(1:non_zero_ham_elem) )
  allocate( ham_elem_array(1:non_zero_ham_elem) )

! 7. reading the rest of the "coo_matrix_direct" file.

  do i1 = 1, non_zero_ham_elem

    read(unit=unit_local, rec=i1+1) row_index_array(i1),    &
                                    column_index_array(i1), &
                                    ham_elem_array(i1)          

  end do

! 8. closing the "coo_matrix_direct" file.

  close( unit=unit_local, status="keep" )

! 9. opening the "coo_matrix_i4" file.

  open( unit   = unit_local,           &
        file   = coo_matrix_i4,        &
        form   = "unformatted",        &
        status = "new",                &
        action = "write" )             
 
! 10. writing the head of the "coo_matrix_i4" file.

  rewind(unit=unit_local)
  write(unit=unit_local) dimen_matrix_i4,                  &  ! writing the 1st record(line)
                         dimen_matrix_i4,                  &
                         non_zero_ham_elem

! 11. writing the rest of the "coo_matrix_i4" file.
  
  do i1 = 1, non_zero_ham_elem   ! reading the rest records(lines)

    write(unit=unit_local) int( row_index_array(i1), kind=si ),     &
                           int( column_index_array(i1), kind=si ),  &
                           ham_elem_array(i1)          

  end do

! 12. closing the "coo_matrix_i4" file.

  close( unit=unit_local, status="keep" )

! 13. deallocating and exiting.

  deallocate( row_index_array ) 
  deallocate( column_index_array )
  deallocate( ham_elem_array )

  end subroutine coo_direct_to_coo_i4

!========================================================================================!
! FINI.
!========================================================================================!

end module m_2_coo_direct_to_coo_i4
