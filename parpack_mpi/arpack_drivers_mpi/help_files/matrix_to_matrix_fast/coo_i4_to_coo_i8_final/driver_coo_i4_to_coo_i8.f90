
program driver_coo_i4_to_coo_i8

  use m_1_type_definitions
  use m_2_coo_i4_to_coo_i8

  implicit none

! 1. input to the subroutine.

  character(len=200) :: coo_matrix_half_i4
  character(len=200) :: coo_matrix_half_i8

!========================================================================================
! 2. the executable part begins here.
!========================================================================================

! 3. some typical names for the files.

  coo_matrix_half_i4 = "coo_matrix_i4"
  coo_matrix_half_i8 = "coo_matrix_i8"

! 4. calling the subroutine.

  call coo_i4_to_coo_i8( coo_matrix_half_i4,  coo_matrix_half_i8 )

end program driver_coo_i4_to_coo_i8
