!====================================================================!
!                                                                    !
! author: pavlos g. galiatsatos.                                     !
!                                                                    !
! date: 2010/11/22.                                                  !
!                                                                    !
! infos: this module is a black-box module.                          !
!        you do not need to adjust anything.                         ! 
!====================================================================!

module m_2_coo_i4_to_coo_i8

  use m_1_type_definitions

  implicit none

  contains
  
!===================================================================================================! --> 1.
!===================================================================================================!
     
!=======================================================================================!
!                                                                                       !
! subroutine: "coo_i4_to_coo_i8".                                                       !
!                                                                                       !
! a. input:                                                                             !
!                                                                                       !
! character(len=200), intent(in) ::                                                     !
!                                                                                       !
! b. output:                                                                            !
!                                                                                       !
! c. purpose:                                                                           !
!                                                                                       !
!                                                                                       !
!=======================================================================================!

  subroutine coo_i4_to_coo_i8( coo_matrix_i4, coo_matrix_i8 )

  implicit none

! 1. inputs

  character(len=200), intent(in) :: coo_matrix_i4
  character(len=200), intent(in) :: coo_matrix_i8

! 2. local variables.

  integer(kind=si) :: dimen_matrix
  integer(kind=di) :: dimen_matrix_i8
  integer(kind=di) :: non_zero_ham_elem
  integer(kind=si), allocatable, dimension(:) :: row_index_array
  integer(kind=si), allocatable, dimension(:) :: column_index_array
  real(kind=dr),    allocatable, dimension(:) :: ham_elem_array
  integer(kind=si), parameter :: unit_local = 11
  integer(kind=di) :: i1

!====================================================================!
! 3. executable part begins here.
!====================================================================!

! 4. opening the "coo_matrix" file.

  open( unit=unit_local,             &
        file=coo_matrix_i4,          &
        form="unformatted",          &
        status="old",                &
        action="read" ) 
 
! 5. reading the head of the "coo_matrix" file.

  rewind(unit=unit_local)
  read(unit=unit_local) dimen_matrix, dimen_matrix, non_zero_ham_elem

  dimen_matrix_i8 = int( dimen_matrix, kind=di )

! 6. allocating ram space.

  allocate( row_index_array(1:non_zero_ham_elem) ) 
  allocate( column_index_array(1:non_zero_ham_elem) )
  allocate( ham_elem_array(1:non_zero_ham_elem) )

! 7. reading the rest of the "coo_matrix" file.

  do i1 = 1, non_zero_ham_elem

    read(unit=unit_local) row_index_array(i1),    &
                          column_index_array(i1), &
                          ham_elem_array(i1)          

  end do

! 8. closing the "coo_matrix_i4" file.

  close( unit=unit_local, status="keep" )

! 9. opening the "coo_matrix_i8" file.

  open( unit=unit_local,             &
        file=coo_matrix_i8,          &
        form="unformatted",          &
        status="new",                &
        action="write" )             
 
! 10. writing the head of the "coo_matrix_direct_i8" file.

  rewind(unit=unit_local)
  write(unit=unit_local)   dimen_matrix_i8,                     &  ! writing the 1st record(line)
                           dimen_matrix_i8,                     &
                           non_zero_ham_elem

! 11. writing the rest of the "coo_matrix_direct_i8" file.
  
  do i1 = 1, non_zero_ham_elem   ! reading the rest records(lines)

    write(unit=unit_local) int( row_index_array(i1), kind=di ),     &
                           int( column_index_array(i1), kind=di ),  &
                           ham_elem_array(i1)          

  end do

! 12. closing the "coo_matrix_i8" file.

  close( unit=unit_local, status="keep" )

! 13. deallocating and exiting.

  deallocate( row_index_array ) 
  deallocate( column_index_array )
  deallocate( ham_elem_array )

  end subroutine coo_i4_to_coo_i8

!========================================================================================!
! FINI.
!========================================================================================!

end module m_2_coo_i4_to_coo_i8
