!====================================================================!
!                                                                    !
! author: pavlos g. galiatsatos.                                     !
!                                                                    !
! date: 2010/11/22.                                                  !
!                                                                    !
! infos: this module is a black-box module.                          !
!        you do not need to adjust anything.                         ! 
!====================================================================!

module m_2_coo_i4_to_coo_direct

  use m_1_type_definitions

  implicit none

  contains
  
!===================================================================================================! --> 1.
!===================================================================================================!
     
!=======================================================================================!
!                                                                                       !
! subroutine: "coo_i4_to_coo_direct".                                                   !
!                                                                                       !
! a. input:                                                                             !
!                                                                                       !
! character(len=200), intent(in) ::                                                     !
!                                                                                       !
! b. output:                                                                            !
!                                                                                       !
! c. purpose:                                                                           !
!                                                                                       !
!                                                                                       !
!=======================================================================================!

  subroutine coo_i4_to_coo_direct( coo_matrix_i4, coo_matrix_direct )

  implicit none

! 1. inputs

  character(len=200), intent(in) :: coo_matrix_i4
  character(len=200), intent(in) :: coo_matrix_direct

! 2. local variables.

  integer(kind=si) :: dimen_matrix_i4
  real(kind=dr) :: dimen_matrix_direct

  integer(kind=di) :: non_zero_ham_elem

  integer(kind=si), allocatable, dimension(:) :: row_index_array
  integer(kind=si), allocatable, dimension(:) :: column_index_array
  real(kind=dr),    allocatable, dimension(:) :: ham_elem_array
  integer(kind=si), parameter :: unit_local = 11
  integer(kind=di) :: i1
  integer(kind=si) :: rec_len
  real(kind=dr), dimension(3) :: dimen_line

!====================================================================!
! 3. executable part begins here.
!====================================================================!

! 4. opening the "coo_matrix" file.

  open( unit=unit_local,             &
        file=coo_matrix_i4,          &
        form="unformatted",          &
        status="old",                &
        action="read" ) 
 
! 5. reading the head of the "coo_matrix" file.

  rewind(unit=unit_local)
  read(unit=unit_local) dimen_matrix_i4, dimen_matrix_i4, non_zero_ham_elem

  dimen_matrix_direct = real( dimen_matrix_i4, kind=dr )

! 6. allocating ram space.

  allocate( row_index_array(1:non_zero_ham_elem) ) 
  allocate( column_index_array(1:non_zero_ham_elem) )
  allocate( ham_elem_array(1:non_zero_ham_elem) )

! 7. reading the rest of the "coo_matrix" file.

  do i1 = 1, non_zero_ham_elem

    read(unit=unit_local) row_index_array(i1),    &
                          column_index_array(i1), &
                          ham_elem_array(i1)          

  end do

! 8. closing the "coo_matrix" file.

  close( unit=unit_local, status="keep" )

! 9. opening the "coo_matrix_direct_i8" file.

  inquire( iolength=rec_len ) dimen_line ! evaluating the record length

  open( unit=unit_local,             &
        file=coo_matrix_direct,      &
        form="unformatted",          &
        status="new",                &
        action="write",              &
        access="direct",             &
        recl=rec_len ) 
 
! 10. writing the head of the "coo_matrix_direct_i8" file.

  rewind(unit=unit_local)
  write(unit=unit_local, rec=1 ) dimen_matrix_direct,                &  ! writing the 1st record(line)
                                 dimen_matrix_direct,                &
                                 real( non_zero_ham_elem, kind=dr )

! 11. writing the rest of the "coo_matrix_direct" file.
  
  do i1 = 1, non_zero_ham_elem   ! reading the rest records(lines)

    write(unit=unit_local, rec=i1+1) real( row_index_array(i1), kind=dr ),     &
                                     real( column_index_array(i1), kind=dr ),  &
                                     ham_elem_array(i1)          

  end do

! 12. closing the "coo_matrix_direct" file.

  close( unit=unit_local, status="keep" )

! 13. deallocating and exiting.

  deallocate( row_index_array ) 
  deallocate( column_index_array )
  deallocate( ham_elem_array )

  end subroutine coo_i4_to_coo_direct

!========================================================================================!
! FINI.
!========================================================================================!

end module m_2_coo_i4_to_coo_direct
