
program driver_fort_to_coo_disk_half_i8

  use m_1_type_definitions
  use m_2_fort_to_coo_disk_half_i8

  implicit none

! 1. the input variables for the subroutine:
!    "matrix_fort_to_coo_official_disk_half".

  character(len=200) :: file_name_from_scatci
  character(len=200) :: file_format_out
  character(len=200) :: file_name_coo_out

!========================================================================================
! 2. defining the input variables for the driver.

  file_name_from_scatci = "fort.26_i8"
  file_format_out       = "unformatted"
  file_name_coo_out     = "matrix_coo_half_i8"

!========================================================================================

! 3. calling the subroutine.

  call matrix_fort_to_coo_disk_half_i8( file_name_from_scatci,    &
                                        file_format_out,          &
                                        file_name_coo_out )

end program driver_fort_to_coo_disk_half_i8
