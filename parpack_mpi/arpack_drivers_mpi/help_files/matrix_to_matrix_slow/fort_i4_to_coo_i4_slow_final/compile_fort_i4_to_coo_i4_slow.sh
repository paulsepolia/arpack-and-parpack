#!/bin/bash

# 1. compiling the code

  ifort  -W1 -O3 -static-intel                       \
  m_1_type_definitions.f90                           \
  m_2_fort_i4_to_coo_i4_slow.f90                     \
  driver_fort_i4_to_coo_i4_slow.f90                  \
  -o x_fort_i4_to_coo_i4_slow
 
# 2. cleaning the *.mod produced files.

  rm *.mod

# 3. exiting.
