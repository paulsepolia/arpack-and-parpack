!====================================================================!
!                                                                    !
! author: pavlos g. galiatsatos.                                     !
!                                                                    !
! date: 2011/04/22.                                                  !
!                                                                    !
! infos: this module is a black-box module.                          !
!        you do not need to adjust anything.                         ! 
!====================================================================!

module m_2_fort_i8_to_coo_direct_slow

  use m_1_type_definitions

  implicit none

  contains
  
!===================================================================================================! --> 1.
!===================================================================================================!
     
!=======================================================================================!
!                                                                                       !
! subroutine: "fort_i8_reading".                                                        !
!                                                                                       !
! a. input:                                                                             !
!                                                                                       !
! character(len=200), intent(in) :: file_name_from_scatci                               !
!                                                                                       !
! b. output:                                                                            !
!                                                                                       !
! integer(kind=di), intent(out) :: dimen_matrix                                         !
! integer(kind=di), intent(out) :: non_zero_ham_elem_half                               !
!                                                                                       !
!=======================================================================================!

  subroutine fort_i8_reading( file_name_from_scatci,   &
                              dimen_matrix,            &
                              non_zero_ham_elem_half,  &
                              num_total_buffers,       &
                              num_buffer_last,         &
                              num_buffer )

  implicit none

! 1.
! a. "dimen_matrix" is the dimension of the hamiltonian matrix.
!    the default type in scatci is single integer precision.
!                                                                                         
! b. "non_zero_ham_elem_half" is the total numbers of non-zero
!    hamiltonian elements of the lower(upper) triangle only.
!    the kind type is double precision  integer, since this variable
!    is getting large values. this fact does not confilct with the
!    kind type definition of the same variable inside scatci,
!    which is single precision integer.
!
! c. these variables have intent(out) attribute, and they are exported
!    to any subroutine which diagonalizes the hamiltonian matrix.

  integer(kind=di), intent(out) :: dimen_matrix
  integer(kind=di), intent(out) :: non_zero_ham_elem_half
  integer(kind=di), intent(out) :: num_total_buffers
  integer(kind=di), intent(out) :: num_buffer
  integer(kind=di), intent(out) :: num_buffer_last

! 2. this variable has intent(in) attribute and is the only input to
!    the "matrix_coo_ram_half" subroutine.

  character(len=200), intent(in) :: file_name_from_scatci

! 3. the input unit.
 
  integer(kind=si), parameter :: matrix_from_scatci = 11

! 4. declaration of some temporary help variables.
!
! a. "i1" is a loop variable.
!
! b. "itmp1" is to store temporarly the buffers. there is no any
!    further process of those values. it is a trash variable.
!
! c. "error_check" is to determine when the reading has
!    reached the end of the "fort.26" file and to exit the
!    infinite reading do-loop. it is a must variable.

  integer(kind=di) :: i1 
  integer(kind=di) :: itmp1
  integer(kind=di) :: error_check

! 5.
!
! a. "num_buffer" and "num_buffer_tmp" are variables for the
!    first value buffer and for all the rest, correspondingly.
!
! b. "num_buffer_last" is the dimension of the last buffer.
!
! c. "num_total_buffers" is the total number of buffers inside the "fort.26" file.
!
! d. these variables are local to the subroutine.
 
  integer(kind=di) :: num_buffer_tmp

! 6. opening the unit.
!
! a. "matrix_from_scatci" is the unit opened for reading
!    the original "fort.26" scatci produced unformatted file.
 
  open( unit=matrix_from_scatci,     &
        file=file_name_from_scatci,  &
        form="unformatted",          &
        status="old",                &
        action="read" ) 

! 7. reading the dimension of the matrix and the buffer length.
!
! a. first we rewind the "matrix_from_scatci" unit.
!
! b. "dimen_matrix" is the dimension of the hamiltonian matrix.  
!
! c. "num_buffer" is the length of the main buffer.
!    the length of the last buffer will be computed later.
 
  rewind(unit=matrix_from_scatci)
  read(unit=matrix_from_scatci) dimen_matrix, num_buffer 

! 8. evaluating the length of the last buffer and the number of the total buffers.
!
! a. "num_buffer_last" is the length of the last buffer.
!
! b. "num_total_buffers" is the variable which its final value
!    is the total number of buffers inside the "fort.26" file.

  num_total_buffers=0 ! setting to zero.

  do ! do-loop: alpha.

    num_total_buffers=num_total_buffers+1

    read(unit=matrix_from_scatci, iostat=error_check) num_buffer_tmp

    if ((num_buffer_tmp /= num_buffer) .and. (error_check == 0_di)) then ! if: alpha.

      num_buffer_last=num_buffer_tmp
 
      exit 

    else if (error_check /= 0_di) then ! else-if: alpha.

      exit

    end if ! end if: alpha.

  end do  ! end of do-loop: alpha.

! 9. evaluating the number of non-zero hamiltonian matrix elements.
!
! a. "non_zero_ham_elem_half" is the number of total upper(lower)
!    triangle non-zero matrix hamiltonian elements.
 
  non_zero_ham_elem_half = (num_total_buffers-1)*num_buffer + num_buffer_last

! 10. closing the unit and keeping the file.

  close(unit=matrix_from_scatci, status="keep")

  end subroutine fort_i8_reading

!===================================================================================================! --> 2.
!===================================================================================================!

!=======================================================================================!
!                                                                                       !
! subroutine: "fort_i8_to_coo_direct_slow".                                             !
!                                                                                       !
! a. input:                                                                             !
!                                                                                       !
! character(len=200), intent(in) :: file_name_from_scatci                               !
! character(len=200), intent(in) :: file_name_coo_direct                                !
!                                                                                       !
!=======================================================================================!

  subroutine fort_i8_to_coo_direct_slow( file_name_from_scatci,   &
                                         file_name_coo_direct )

  implicit none

! 1.

  character(len=200), intent(in) :: file_name_from_scatci
  character(len=100), intent(in) :: file_name_coo_direct

! 2.
 
  integer(kind=si), parameter :: matrix_from_scatci = 113
  integer(kind=si), parameter :: matrix_coo_direct  = 114

! 3. help variables

  integer(kind=di) :: dimen_matrix
  integer(kind=di) :: non_zero_ham_elem_half
  integer(kind=di) :: i1
  integer(kind=si) :: i2
  integer(kind=di) :: i3 
  integer(kind=di) :: itmp1
  integer(kind=di) :: error_check
  integer(kind=di) :: num_buffer
  integer(kind=di) :: num_buffer_last
  integer(kind=di) :: num_total_buffers 
  integer(kind=di) :: rec_len
  real(kind=dr), dimension(3) :: dimen_line 
  integer(kind=di), allocatable, dimension(:,:) :: ij_tmp
  real(kind=dr),    allocatable, dimension(:)   :: emx_tmp 

! 4. opening the units.

  call fort_i8_reading( file_name_from_scatci,   &
                        dimen_matrix,            &
                        non_zero_ham_elem_half,  &
                        num_total_buffers,       &
                        num_buffer_last,         &
                        num_buffer )


  open( unit=matrix_from_scatci,     &
        file=file_name_from_scatci,  &
        form="unformatted",          &
        status="old",                &
        action="read" ) 

  inquire( iolength=rec_len ) dimen_line ! evaluating the record length

  open(unit=matrix_coo_direct,     &
       file=file_name_coo_direct,  &
       form="unformatted",         &
       status="new",               &
       action="write",             &
       access="direct",            &
       recl=rec_len)

 
! 7. allocating the temporary arrays.
!
! a. an important note is that the last "ij_tmp" must
!    have the common buffer length size. otherwise the
!    reading "fort.26" process fails. this is because
!    in such a way the "fort.26" is built inside scatci.
!
! b. the following arrays are tmp.
!    they are deallocated as soon as they become useless.

  allocate(ij_tmp(1:2,1:num_buffer)) 
  allocate(emx_tmp(1:num_buffer))    

! 8. rewinding the "matrix_from_scatci" unit.
!
! a. the purpose is to rewind the unit and to skip the first line.

  rewind(unit=matrix_from_scatci) 
  read(unit=matrix_from_scatci) 

! 9. reading/writing all the buffers except the last and writing the infos.
!
! a. the purpose is to read all the elements up to the previous
!    to the last buffer of "fort.26" file, and on the fly, for
!    each buffer, to store its infos to the non-local arrays
!    "row_index_array", "column_index_array", "ham_elem_array".

 write(unit=matrix_coo_direct,rec=1) real( dimen_matrix, kind=dr ),            &
                                     real( dimen_matrix, kind=dr ),            &
                                     real( non_zero_ham_elem_half, kind=dr )

 i3 = 2_di                                     

 do i1=1, num_total_buffers-1 ! do-loop: beta.

    read(unit=matrix_from_scatci) itmp1, ij_tmp(:,:), emx_tmp(:)

    do i2 = 1, num_buffer

      write(unit=matrix_coo_direct,rec=i3) real( ij_tmp(1,i2), kind=dr ),            &
                                           real( ij_tmp(2,i2), kind=dr ),            &
                                           real( emx_tmp(i2) , kind=dr )

      i3 = i3 + 1

    end do

 end do ! end of do-loop: beta.

! 10. reading/writing the last buffer and writting the infos.
!
! a. the purpose is to read the last buffer and to put the data
!    to the arrays "row_index_array", "column_index_array" and
!    "ham_elem_array".

  read(unit=matrix_from_scatci) itmp1, ij_tmp(:,:), emx_tmp(:)

  do i2 = 1, num_buffer_last

    write(unit=matrix_coo_direct,rec=i3) real( ij_tmp(1,i2), kind=dr ),            &
                                         real( ij_tmp(2,i2), kind=dr ),            &
                                         real( emx_tmp(i2) , kind=dr )

    i3 = i3 + 1

  end do

! 11. deallocating the tmp help arrays.
!
! a. the "ij_tmp(:,:)" and "emx_tmp(:)" arrays are no more needed.

  deallocate(ij_tmp)  
  deallocate(emx_tmp) 

! 12. closing the units and keeping the files.

  close(unit=matrix_from_scatci, status="keep")
  close(unit=matrix_coo_direct, status="keep")

  end subroutine fort_i8_to_coo_direct_slow

end module m_2_fort_i8_to_coo_direct_slow

!===================================================================================================!
! FINI.                                                                                             !
!===================================================================================================!
