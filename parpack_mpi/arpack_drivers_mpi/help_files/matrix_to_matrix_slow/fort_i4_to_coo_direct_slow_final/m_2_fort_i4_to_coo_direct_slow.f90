!====================================================================!
!                                                                    !
! author: pavlos g. galiatsatos.                                     !
!                                                                    !
! date: 2011/04/21.                                                  !
!                                                                    !
! infos: this module is a black-box module.                          !
!        you do not need to adjust anything.                         ! 
!====================================================================!

module m_2_fort_i4_to_coo_direct_slow

  use m_1_type_definitions

  implicit none

  contains
  
!===================================================================================================! --> 1.
!===================================================================================================!
     
!=======================================================================================!
!                                                                                       !
! subroutine: "fort_i4_to_coo_direct_slow".                                             !
!                                                                                       !
! a. input:                                                                             !
!                                                                                       !
! character(len=200), intent(in) :: file_name_from_scatci                               !
! character(len=200), intent(in) :: file_name_coo_direct                                !
!                                                                                       !
!=======================================================================================!

  subroutine fort_i4_to_coo_direct_slow( file_name_from_scatci,   &
                                         file_name_coo_direct )

  implicit none

! 1.

  character(len=200), intent(in) :: file_name_from_scatci
  character(len=100), intent(in) :: file_name_coo_direct

! 2.
 
  integer(kind=si), parameter :: matrix_from_scatci = 113
  integer(kind=si), parameter :: matrix_coo_direct  = 114

! 3. help variables

  integer(kind=si) :: dimen_matrix
  integer(kind=di) :: non_zero_ham_elem_half
  integer(kind=di) :: i1
  integer(kind=si) :: i2
  integer(kind=di) :: i3 
  integer(kind=si) :: itmp1
  integer(kind=si) :: error_check
  integer(kind=si) :: num_buffer
  integer(kind=si) :: num_buffer_tmp
  integer(kind=si) :: num_buffer_last
  integer(kind=si) :: num_total_buffers 
  integer(kind=si) :: rec_len
  real(kind=dr), dimension(3) :: dimen_line 
  integer(kind=si), allocatable, dimension(:,:) :: ij_tmp
  real(kind=dr),    allocatable, dimension(:)   :: emx_tmp 

! 4. opening the units.
 
  open( unit=matrix_from_scatci,     &
        file=file_name_from_scatci,  &
        form="unformatted",          &
        status="old",                &
        action="read" ) 

  inquire( iolength=rec_len ) dimen_line ! evaluating the record length

  open(unit=matrix_coo_direct,     &
       file=file_name_coo_direct,  &
       form="unformatted",         &
       status="new",               &
       action="write",             &
       access="direct",            &
       recl=rec_len)

! 5. reading the dimension of the matrix and the buffer length.
!
! a. first we rewind the "matrix_from_scatci" unit.
!
! b. "dimen_matrix" is the dimension of the hamiltonian matrix.  
!
! c. "num_buffer" is the length of the main buffer.
!    the length of the last buffer will be computed later.
 
  rewind(unit=matrix_from_scatci)
  read(unit=matrix_from_scatci) dimen_matrix, num_buffer 

! 6. evaluating the length of the last buffer and the number of the total buffers.
!
! a. "num_buffer_last" is the length of the last buffer.
!
! b. "num_total_buffers" is the variable which its final value
!    is the total number of buffers inside the "fort.26" file.

  num_total_buffers=0 ! setting to zero.

  do ! do-loop: alpha.

    num_total_buffers=num_total_buffers+1

    read(unit=matrix_from_scatci, iostat=error_check) num_buffer_tmp

    if ((num_buffer_tmp /= num_buffer) .and. (error_check == 0)) then ! if: alpha.

      num_buffer_last=num_buffer_tmp
 
      exit 

    else if (error_check /= 0) then ! else-if: alpha.

      exit

    end if ! end if: alpha.

  end do  ! end of do-loop: alpha.
 
  non_zero_ham_elem_half = (num_total_buffers-1)*num_buffer + num_buffer_last
 
! 7. allocating the temporary arrays.
!
! a. an important note is that the last "ij_tmp" must
!    have the common buffer length size. otherwise the
!    reading "fort.26" process fails. this is because
!    in such a way the "fort.26" is built inside scatci.
!
! b. the following arrays are tmp.
!    they are deallocated as soon as they become useless.

  allocate(ij_tmp(1:2,1:num_buffer)) 
  allocate(emx_tmp(1:num_buffer))    

! 8. rewinding the "matrix_from_scatci" unit.
!
! a. the purpose is to rewind the unit and to skip the first line.

  rewind(unit=matrix_from_scatci) 
  read(unit=matrix_from_scatci) 

! 9. reading/writing all the buffers except the last and writing the infos.
!
! a. the purpose is to read all the elements up to the previous
!    to the last buffer of "fort.26" file, and on the fly, for
!    each buffer, to store its infos to the non-local arrays
!    "row_index_array", "column_index_array", "ham_elem_array".

 write(unit=matrix_coo_direct,rec=1) real( dimen_matrix, kind=dr ),            &
                                     real( dimen_matrix, kind=dr ),            &
                                     real( non_zero_ham_elem_half, kind=dr )

 i3 = 2_di                                     

 do i1=1, num_total_buffers-1 ! do-loop: beta.

    read(unit=matrix_from_scatci) itmp1, ij_tmp(:,:), emx_tmp(:)

    do i2 = 1, num_buffer

      write(unit=matrix_coo_direct,rec=i3) real( ij_tmp(1,i2), kind=dr ),            &
                                           real( ij_tmp(2,i2), kind=dr ),            &
                                           real( emx_tmp(i2) , kind=dr )

      i3 = i3 + 1

    end do

 end do ! end of do-loop: beta.

! 10. reading/writing the last buffer and writting the infos.
!
! a. the purpose is to read the last buffer and to put the data
!    to the arrays "row_index_array", "column_index_array" and
!    "ham_elem_array".

  read(unit=matrix_from_scatci) itmp1, ij_tmp(:,:), emx_tmp(:)

  do i2 = 1, num_buffer_last

    write(unit=matrix_coo_direct,rec=i3) real( ij_tmp(1,i2), kind=dr ),            &
                                         real( ij_tmp(2,i2), kind=dr ),            &
                                         real( emx_tmp(i2) , kind=dr )

    i3 = i3 + 1

  end do

! 11. deallocating the tmp help arrays.
!
! a. the "ij_tmp(:,:)" and "emx_tmp(:)" arrays are no more needed.

  deallocate(ij_tmp)  
  deallocate(emx_tmp) 

! 12. closing the units and keeping the files.

  close(unit=matrix_from_scatci, status="keep")
  close(unit=matrix_coo_direct, status="keep")

  end subroutine fort_i4_to_coo_direct_slow

!===================================================================================================!
! FINI.                                                                                             !
!===================================================================================================!

end module m_2_fort_i4_to_coo_direct_slow
