#!/bin/bash

# 1. compiling the code

  ifort -O3 -static-intel                         \
  -parallel -par-threshold0                       \
  -par-report1                                    \
  -mkl=parallel                                   \
  -openmp                                         \
  -openmp-report2                                 \
  m_1_type_definitions.f90                        \
  m_2_arp_coo_half_disk_i4.f90                    \
  m_3_arp_coo_half_omp.f90                        \
  -L. -larpack_pmkl                               \
  driver_arp_coo_half_omp.f90                     \
  -o x_arpack
 
# 2. cleaning the *.mod produced files.

  rm *.mod

# 3. exiting.
