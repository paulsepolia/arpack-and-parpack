
program driver_arp_coo_half_omp

  use m_1_type_definitions
  use m_2_arp_coo_half_disk_i4
  use m_3_arp_coo_half_omp

  implicit none

! 1. variables ONLY for the subroutine:
!    "matrix_coo_official_to_ram_half".

  character(len=200) :: matrix_file_name
  character(len=200) :: type_of_format_for_matrix

! 2. variables COMMON to the subroutines:
!    "matrix_coo_official_to_ram_half"  
!      and
!    "arpack_coo_half".

  integer(kind=si) :: matrix_dimen
  integer(kind=di) :: non_zero_ham_elem
  integer(kind=si), allocatable, dimension(:) :: row_index_array
  integer(kind=si), allocatable, dimension(:) :: column_index_array
  real(kind=dr)   , allocatable, dimension(:) :: ham_elem_array

! 3. variables ONLY for the subroutine: 
!    "arpack_coo_half".

  real(kind=dr), allocatable, dimension(:,:) :: vectors_array
  real(kind=dr), allocatable, dimension(:,:) :: values_array
  integer(kind=si) :: nev     
  integer(kind=si) :: maxitr
  real(kind=dr)    :: tol 
  character(len=2) :: which
  integer(kind=si) :: ncv_coeff
  integer(kind=si) :: nt

! 4. local variables for outputting the produced eigensystem. 

  character(len=200) :: type_of_format_for_output
  character(len=200) :: file_for_eigenvalues
  character(len=200) :: file_for_eigenvectors
 
! 5. local variables and parameters.
  
  real(kind=dr), parameter :: zero = 0.0_dr
  integer(kind=si) :: i1, i2

!========================================================================================!
!========================================================================================!
! 6. All the necessary input for the driver.

  ! 6-1. name of the matrix file in hard disk.
  matrix_file_name = "matrix_coo_half" 

  ! 6-2. type of format of the matrix file.
  type_of_format_for_matrix = "unformatted"

  ! 6-3. type of format of the output eigensystem.
  type_of_format_for_output = "formatted"

  ! 6-4. name of the file for eigenvalues.
  file_for_eigenvalues = "___va"

  ! 6-5. name of the file for eigenvectors.
  file_for_eigenvectors = "___ve"

  ! 6-6. number of eigenvalues/eigenvectors to get back.
  nev = 200

  ! 6-7. maximum arnoldi restarts.
  maxitr = 3000000

  ! 6-8. the convergence criterion.
  tol = zero 
  
  ! 6-9. the part of the spectrum.
  which = 'SA'

  ! 6-10. the part 'ncv_coeff'.
  ncv_coeff = 2
 
  ! 6-11. the number of openmp cpus.
  nt = 2
 

!========================================================================================!
!========================================================================================!

! 7. calling the subroutine which reads form the hard disk the matrix: 'matrix_file_name'
!    and writes in to the ram the variables scalar/arrays:
!
!    a. dimension of the matrix                     : "matrix_dimen".
!    b. the number of non zero hamiltonian elements : "non_zero_ham_elem".
!    c. the indexes of rows                         : "row_index_array".
!    d. the indexes of columns                      : "column_index_array".
!    e. the corresponing hamiltonian elements       : "ham_elm_array"  

  call  matrix_coo_half_to_ram( matrix_file_name,           &
                                type_of_format_for_matrix,  &
                                matrix_dimen,               &
                                non_zero_ham_elem,          &
                                row_index_array,            &
                                column_index_array,         &
                                ham_elem_array )

! 8. testing. outputting some characteristics of the matrix: "matrix_file_name".

  write(*,*) "==========================================================================="
  write(*,*) "==========================================================================="
  write(*,*) " Inside driver program."
  write(*,*) 
  write(*,*) " Some characteristics values of the matrix under diagonalization."
  write(*,*) 
  write(*,*) " matrix_dimen --> ", matrix_dimen
  write(*,*) " non_zero_ham_elem --> ", non_zero_ham_elem
  write(*,*) " row_index_array(100) --> ", row_index_array(100)
  write(*,*) " column_index_array(100) --> ", column_index_array(100)
  write(*,*) " ham_elem_array(100) --> ", ham_elem_array(100)
  write(*,*) 
  write(*,*) "==========================================================================="


! 9. calling the subroutine which diagonalizes the matrix: "matrix_file_name".

  call arp_coo_half_omp( row_index_array,      &  !  1. input & deallocated.
                         column_index_array,   &  !  2. input & deallocated.
                         ham_elem_array,       &  !  3. input & deallocated.
                         matrix_dimen,         &  !  4. input & output.
                         non_zero_ham_elem,    &  !  5. input & output.
                         which,                &  !  6. input.
                         ncv_coeff,            &  !  7. input.
                         nev,                  &  !  8. input & output.
                         maxitr,               &  !  9. input & output.
                         tol,                  &  !  10. input & output.
                         vectors_array,        &  !  11. output.
                         values_array )           !  12. output.

 
! 10. writing to the hard disk the eigensystem.

  if ( type_of_format_for_output == "unformatted" ) then

    ! 1. eigenvalues area. unformatted output.
    open( unit=11,                             &
          file=file_for_eigenvalues,           &
          status="new",                        &
          action="write",                      &
          form=type_of_format_for_output )

    do i1 = 1, nev

      write(unit=11) values_array(i1,1)

    end do
  
    close( unit=11,                            &
           status="keep" )

    ! 2. eigenvectors area. unformatted output.
    open( unit=11,                             &
          file=file_for_eigenvectors,          &
          status="new",                        &
          action="write",                      &
          form=type_of_format_for_output )


    do i1 = 1, nev
      do i2 = 1, matrix_dimen

        write(unit=11) vectors_array(i2,i1)

      end do
    end do

    close( unit=11,                            &
           status="keep" )

  else if ( type_of_format_for_output == "formatted" ) then

    ! 3. eigenvalues area. formatmatted output
    open( unit=11,                             &
          file=file_for_eigenvalues,           &
          status="new",                        &
          action="write",                      &
          form=type_of_format_for_output )

    do i1 = 1, nev

      write(unit=11,fmt=*) values_array(i1,1)

    end do

    close( unit=11,                            &
           status="keep" )

    ! 4. eigenvectors area. formatted output.
    open( unit=11,                             &
          file=file_for_eigenvectors,          &
          status="new",                        &
          action="write",                      &
          form=type_of_format_for_output )


    do i1 = 1, nev
      do i2 = 1, matrix_dimen

        write(unit=11,fmt=*) vectors_array(i2,i1)

      end do
    end do

  else 

    write(*,*) " error: type of format must be 'formatted' of 'unformatted' "
    stop   

  end if

! 10. deallocations.

  deallocate(row_index_array)
  deallocate(column_index_array)
  deallocate(ham_elem_array)
  deallocate(values_array)
  deallocate(vectors_array)

end program driver_arp_coo_half_omp
