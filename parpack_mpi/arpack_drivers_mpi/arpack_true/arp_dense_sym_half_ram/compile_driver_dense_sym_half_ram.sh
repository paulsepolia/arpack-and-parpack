#!/bin/bash

# 1. compiling the code

  ifort -O3 -static-intel                          \
  -parallel -par-threshold0                        \
  -par-report1                                     \
  -mkl=parallel                                    \
  -openmp                                          \
  -openmp-report2                                  \
  m_1_type_definitions.f90                         \
  m_3_mat_dense_half_vec_product.f90               \
  m_4_arpack_dense_sym_half.f90                    \
  driver_arpack_dense_sym_half_ram.f90             \
  -L. -larpack_pmkl                                \
  -o x_arpack
 
# 2. cleaning the *.mod produced files.

  rm *.mod

# 3. exiting.
