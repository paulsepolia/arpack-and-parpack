#!/bin/bash

# 1. compiling

  ifort -static-intel                        \
  m_1_type_definitions.f90                   \
  driver_read_vectors.f90                    \
  -o x_read_vectors

# 2. cleaning 

  rm *.mod
