!====================================================================
! Author: Pavlos G. Galiatsatos.
! Date: 2011/09/19.
! Help program to read the direct access 
! output file "___ve_mpi" and to write it
! in to the standard output unit.
! Developed not only for debuging purposes.
!====================================================================

   program driver_read_vectors

   use m_1_type_definitions

   implicit none

   real(kind=dr), dimension(1) :: dimen_line
   integer(kind=di)            :: rec_len
   real(kind=dr)               :: x
   integer(kind=di)            :: i1, i2, i3
   integer(kind=di)            :: tot
   integer(kind=si)            :: p
   integer(kind=si)            :: nev
   integer(kind=si)            :: matrix_dimen
   integer(kind=si)            :: ex_elem

   ! a.

   open( unit=13,                                    &
         file="___tmp_details",                      &
         form="unformatted",                         &
         status="old",                               &
         action="read" )

   read( unit=13 ) p
   read( unit=13 ) nev
   read( unit=13 ) matrix_dimen
   read( unit=13 ) ex_elem

   close( unit=13, status="delete" )

   ! b.

   inquire( iolength = rec_len ) dimen_line

   open( unit   = 13,             &
         form   = "unformatted",  &
         access = "direct",       &
         file   = "___ve_mpi",    &
         status = "old",          &
         action = "read",         &
         recl   = rec_len )

   tot = nev * (matrix_dimen+ex_elem)

   i3 = 1_di

   do i2 = 1, nev

     do i1 = 1, matrix_dimen

       read( unit=13, rec=i3 ) dimen_line(1)

       x=dimen_line(1)

!       write(unit=33) x      ! Use this line for unformatted output
                              ! Be carefull of the 'unit=33' not to overlap
                              ! with some other file.
       write(*,*) dabs(x)     ! The dabs() function is for debuging purposes

       i3 = i3 + 1_di

     end do  

     i3 = i3 + ex_elem
 
   end do

   close( unit=13, status="delete" )

   end program driver_read_vectors

!==============================================================================
! FINI.
!==============================================================================
