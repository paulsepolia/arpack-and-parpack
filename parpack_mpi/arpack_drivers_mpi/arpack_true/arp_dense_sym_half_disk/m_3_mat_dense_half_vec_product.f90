!==============================================================================!
! Author.
! Pavlos G. Galiatsatos.
! Date: 2011/07/11.
!==============================================================================!

  module m_3_mat_dense_half_vec_product

  use m_1_type_definitions

  implicit none

  contains

!==============================================================================!
!==============================================================================!
!
! 1.
!
! 1a. Subroutine: mv_dense_sym_half( matrix,           &
!                                    matrix_dimen,     & 
!                                    x,                &
!                                    y )           
!
! 1b. This subroutine is an impementation of a 
!     dense-symmetric-matrix-vector product. 
!     The dense matrix is stored as a vector 
!     which contains only the half elements
!     (lower triangle or upper triangle).
!     The purpose of this impementation is to save RAM space
!     and also to speed up the dense-matrix-vector product.
!
! 1c. The following code developed using Mathematica. 
!     Strictly speaking is a copy-paste Mathematica code.
!     
!==============================================================================!
!==============================================================================!

  subroutine mat_dense_half_vec_product( matrix,         & 
                                         matrix_dimen,   &
                                         x,              &
                                         y )

  implicit none

  ! 1. input variables.

  real(kind=dr),    intent(in),  allocatable, dimension(:) :: matrix
  integer(kind=si), intent(in)                             :: matrix_dimen
  real(kind=dr),    intent(in),  allocatable, dimension(:) :: x
  real(kind=dr),    intent(out), allocatable, dimension(:) :: y

  ! 2. local variables.

  integer(kind=di) :: i1, i2, i3
  integer(kind=di) :: dim_total
  integer(kind=si), allocatable, dimension(:) :: peakF
  integer(kind=si), allocatable, dimension(:) :: downF
  real(kind=dr)   , allocatable, dimension(:) :: mat_vec
  integer(kind=si) :: i1_help
  integer(kind=si) :: i1_do, i2_do, i3_do

  ! 3.

  allocate( mat_vec(1:matrix_dimen) )
  allocate( peakF(1:matrix_dimen) )
  allocate( downF(1:matrix_dimen) )
  allocate( y(1:matrix_dimen) )

  ! 4.
  y = 0.0d0
  
  ! a.
  i1 = 0_di
  do i1_do = 1, matrix_dimen

    i1 = i1 + 1_di 
    i1_help = ( (-i1+3)*i1 ) / 2 + matrix_dimen * (i1-1)  
    peakF(i1_do) = i1_help 

  end do

  ! b.
  i1 = 0_di
  do i1_do = 1, matrix_dimen
  
    i1 = i1 + 1_di
    i1_help = ( (1-i1)*i1 ) / 2 + matrix_dimen * (i1) 
    downF(i1_do) = i1_help   

  end do

  ! c.
  i1 = 0_di
  do i1_do = 1, matrix_dimen

    ! c.1.
    i1 = i1 + 1_di
    mat_vec(1) = matrix(i1)

    ! c.2.
    i3 = 0_di
    do i3_do = 1, i1_do-2
      i1 = i1_do
      i3 = i3_do
      mat_vec(i3_do+1) = matrix( i1 + (-i3+2*matrix_dimen*i3-i3**2) / 2 )
    end do

    ! c.3.
    mat_vec(i1_do:matrix_dimen) = matrix( peakF(i1_do) : downF(i1_do) )
 
    ! c.4.
    do i2_do = 1, matrix_dimen
      y(i1_do) = y(i1_do) + mat_vec(i2_do) * x(i2_do)
    end do

  end do

  end subroutine mat_dense_half_vec_product

!==============================================================================!
! FINI.
!==============================================================================!

  end module m_3_mat_dense_half_vec_product

