!=================================================
! Author: Pavlos G. Galiatsatos.
! Date: 2011/07/11.
!=================================================

module m_2_matrix_dense_sym_half_to_ram

  use m_1_type_definitions

  implicit none

  contains

  subroutine matrix_dense_sym_half_to_ram( matrix_file,              &
                                           type_of_format,           & 
                                           matrix_dimen,             &
                                           matrix )
                                    

  implicit none

  ! 1. interface variables.

  character(len=200), intent(inout)                     :: matrix_file
  character(len=200), intent(inout)                     :: type_of_format
  integer(kind=si)  , intent(out)                       :: matrix_dimen
  real(kind=dr), allocatable, dimension(:), intent(out) :: matrix

  ! 2. local variables.

  integer(kind=si) :: i1, i2
  integer(kind=di) :: indx 

  ! 3. opening the matrix-file unit.
  
  open( unit = 11,             &
        form = type_of_format, &
        file = matrix_file,    &
        status = "old",        &
        action = "read" )  

  ! 4. choosing the format and reading the "matrix_dimen" variable.

  if ( type_of_format == "unformatted" ) then 

    read( unit=11 ) matrix_dimen

  else if ( type_of_format == "formatted" ) then

    read( unit=11, fmt=* ) matrix_dimen

  else 

    write(*,*)  " error in the chosen format: ", type_of_format
    write(*,*)  " the chosen format must be 'formatted' or 'unformatted'." 
    stop
 
  end if

  ! 5. allocating ram space for the "matrix".

  allocate( matrix( 1: (int(matrix_dimen,kind=di)**2 + matrix_dimen)/2 ) )
  
  ! 6. reading the matrix elements stored in hard disk.
  
  if ( type_of_format == "unformatted" ) then
  
    indx = 0   

    do i1 = 1, matrix_dimen
      do i2 = i1, matrix_dimen

        indx = indx + 1
        read( unit=11 ) matrix(indx)

      end do
    end do 
  
  else if ( type_of_format == "formatted" ) then

    indx = 0

    do i1 = 1, matrix_dimen
      do i2 = i1, matrix_dimen

        indx = indx + 1 
        read( unit=11, fmt=* ) matrix(indx)

      end do
    end do

  end if

  ! 7. closing the unit and keeping the matrix.
 
  close( unit=11, status="keep" )

  end subroutine matrix_dense_sym_half_to_ram

end module m_2_matrix_dense_sym_half_to_ram
