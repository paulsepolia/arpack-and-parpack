#!/bin/bash

# 1. compiling

	mpif90.mpich2  -O3                            \
  			       m_1_type_definitions.f90       \
  			       m_2_matrix_coo_ram.f90         \
  			       m_3_smv_coo_half_mpi.f90       \
  			       driver_arpack_coo_mpi_ram.f90  \
 				   /opt/mkl/2015/lib/intel64/libmkl_blas95_lp64.a \
                   /opt/mkl/2015/lib/intel64/libmkl_lapack95_lp64.a \
                   /opt/mkl/2015/lib/intel64/libmkl_scalapack_lp64.a \
                   -Wl,--start-group /opt/mkl/2015/lib/intel64/libmkl_gf_lp64.a \
                   /opt/mkl/2015/lib/intel64/libmkl_core.a \
                   /opt/mkl/2015/lib/intel64/libmkl_intel_thread.a -Wl,--end-group \
                   /opt/mkl/2015/lib/intel64/libmkl_blacs_intelmpi_lp64.a \
                   /opt/parpack/lib_2015/libparpack_intel_mkl.a \
                   /opt/parpack/lib_2015/libarpack_intel_mkl.a \
                   -liomp5 -ldl -lpthread -limf \
				   -o x_gnu_mpich2

# 2. cleaning 

  rm *.mod
