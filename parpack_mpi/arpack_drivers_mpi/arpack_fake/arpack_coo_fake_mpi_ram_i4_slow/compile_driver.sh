#!/bin/bash

# 1. compiling

  mpiifort -O3 -static-intel              \
  -mkl=cluster                            \
  m_1_type_definitions.f90                \
  m_2_matrix_coo_ram.f90                  \
  m_3_smv_coo_half_mpi.f90                \
  driver_arpack_coo_mpi_ram.f90           \
  -L. -lparpack_intel_mpi                 \
  -L. -larpack_intel_omp                  \
  -o x_mpi

# 2. cleaning 

  rm *.mod
