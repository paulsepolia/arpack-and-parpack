#!/bin/bash

# 1. compiling

  mpiifort -O2 -static-intel -static_mpi     \
  -mkl=cluster                               \
  m_1_type_definitions.f90                   \
  m_2_disk_direct_to_ram.f90                 \
  m_3_smv_coo_half_mpi.f90                   \
  driver_parpack_direct_mpi.f90              \
  -L. -lparpack_intel_mkl                    \
  -L. -larpack_intel_mkl                     \
  -o x_parpack

# 2. cleaning 

  rm *.mod
