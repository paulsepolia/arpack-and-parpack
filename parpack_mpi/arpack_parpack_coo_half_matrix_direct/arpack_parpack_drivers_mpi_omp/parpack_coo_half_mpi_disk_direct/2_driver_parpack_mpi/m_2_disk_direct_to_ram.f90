!====================================================================!
!                                                                    !
! Author: Pavlos G. Galiatsatos.                                     !
!                                                                    !
! Date: 2011/09/13.                                                  !
!                                                                    !
!====================================================================!

  module m_2_disk_direct_to_ram
 
  use mpi
  use m_1_type_definitions

  implicit none

  contains 

!========================================================================================
! subroutine: direct_disk_to_ram.
!========================================================================================

  subroutine disk_direct_to_ram( comm,                         &     !  1.
                                 my_rank,                      &     !  2.
                                 p,                            &     !  3.
                                 matrix_dimen,                 &     !  4.
                                 non_zero_ham_elem_half,       &     !  5.
                                 row_index_array,              &     !  6.
                                 column_index_array,           &     !  7.
                                 ham_elem_array,               &     !  8.
                                 non_zero_a,                   &     !  9.
                                 matrix_name )                       ! 10.

  ! 1. declarations of the interface variables.

  integer(kind=si),  intent(in)                              :: comm                    !  1.                    
  integer(kind=si),  intent(in)                              :: my_rank                 !  2.
  integer(kind=si),  intent(in)                              :: p                       !  3.
  integer(kind=si),  intent(out)                             :: matrix_dimen            !  4.
  integer(kind=di),  intent(out)                             :: non_zero_ham_elem_half  !  5.
  integer(kind=si),  intent(out), allocatable, dimension(:)  :: row_index_array         !  6.
  integer(kind=si),  intent(out), allocatable, dimension(:)  :: column_index_array      !  7.
  real(kind=dr)   ,  intent(out), allocatable, dimension(:)  :: ham_elem_array          !  8.
  integer(kind=di),  intent(out)                             :: non_zero_a              !  9.
  character(len=200),intent(in)                              :: matrix_name             ! 10.

  ! 2. declaration of the rest variables.

  integer(kind=di)             :: mod_a
  integer(kind=di)             :: i
  integer(kind=si)             :: ierr
  real(kind=dr), dimension(3)  :: dimen_line
  integer(kind=si)             :: rec_len

  ! 3. opening the unit to read the size of the matrix
  !    and the number of the non-zero hamiltonian elements.

  inquire( iolength=rec_len ) dimen_line

  if ( my_rank == 0 ) then

    ! 3.a.
    open( unit = 11,                          &
          form = "unformatted",               &
          file = matrix_name,                 &
          status = "old",                     &
          action = "read",                    &
          access = "direct",                  &
          recl = rec_len )

    ! 3.b.
    read( unit=11, rec=1 ) dimen_line(1), dimen_line(2), dimen_line(3)
    
    matrix_dimen           = dimen_line(1)
    matrix_dimen           = dimen_line(2)
    non_zero_ham_elem_half = dimen_line(3)

    ! 3.c.
    close( unit=11, status="keep" )

  end if

  call MPI_Barrier( comm, ierr )

  ! 4. broadcasting the matrix dimension value
  !    and the number of non-zero hamiltonian elements

  call MPI_Bcast( matrix_dimen, 1, MPI_INTEGER, 0, comm, ierr )
  call MPI_Bcast( non_zero_ham_elem_half, 1, MPI_INTEGER8, 0, comm, ierr )
  call MPI_Barrier( comm, ierr )

  ! 5. finding the value of the dimension of the array
  !    that i have to allocate in each mpi thread

  if ( mod( non_zero_ham_elem_half, p ) == 0 ) then
    non_zero_a = non_zero_ham_elem_half / p
    mod_a = 0
  else
    non_zero_a = ( non_zero_ham_elem_half / p ) + 1
    mod_a = mod( non_zero_ham_elem_half, p )
  end if

  ! 6. allocating space for the local arrays
  !    to put the sparse matrix data in each mpi thread

  allocate( row_index_array(1:non_zero_a) )
  allocate( column_index_array(1:non_zero_a) )
  allocate( ham_elem_array(1:non_zero_a) )

  call MPI_Barrier( comm, ierr )

  ! 7. reading the sparse matrix data for rank 0

  if ( my_rank == 0 ) then

    ! 7.a.
    open( unit = 11,                          &
          form = "unformatted",               &
          file = matrix_name,                 &
          status = "old",                     &
          action = "read",                    & 
          access = "direct",                  &
          recl = rec_len )

    ! 7.b. 

    do i = 1, non_zero_a

      read( unit=11, rec = i+1 ) dimen_line(1), dimen_line(2), dimen_line(3)

      row_index_array(i)    = dimen_line(1)  
      column_index_array(i) = dimen_line(2) 
      ham_elem_array(i)     = dimen_line(3)

    end do

    ! 7.c.

    close( unit=11, status="keep" )

  end if

  call MPI_Barrier( comm, ierr )

  ! 8.  reading the sparse matrix data
  !     from and for the rest of mpi threads
  !     which are less than the 'mod_a' variable

  if ( my_rank /= 0 .and. my_rank < mod_a ) then

    ! 8.a.

    open( unit = 11,                          &
          form = "unformatted",               &
          file = matrix_name,                 &
          status = "old",                     &
          action = "read",                    &
          access = "direct",                  &
          recl = rec_len )

    ! 8.b.

    do i = 1, non_zero_a

      read( unit=11, rec = 1 + i + non_zero_a * my_rank ) &
    
      dimen_line(1), dimen_line(2), dimen_line(3)

      row_index_array(i)    = dimen_line(1)  
      column_index_array(i) = dimen_line(2) 
      ham_elem_array(i)     = dimen_line(3)

    end do

    ! 8.c.

    close( unit=11, status="keep" )
  
  end if

  call MPI_Barrier( comm, ierr )

  ! 9. reading the sparse matrix data
  !    from and for the rest of mpi threads
  !    which are greater or equal than 'mod_a' variable

  if ( my_rank /= 0 .and. my_rank >= mod_a ) then

    ! 9.a.

    open( unit = 11,                          &
          form = "unformatted",               &
          file = matrix_name,                 &
          status = "old",                     &
          action = "read",                    &
          access = "direct",                  &
          recl = rec_len )

    ! 9.b.

    do i = 1, non_zero_a - 1

      read( unit=11, rec = 1 + i + non_zero_a*mod_a + (my_rank-mod_a)*(non_zero_a-1) )  &
        dimen_line(1), dimen_line(2), dimen_line(3)

      row_index_array(i)    = dimen_line(1)  
      column_index_array(i) = dimen_line(2) 
      ham_elem_array(i)     = dimen_line(3)

    end do

    ! 9.c.

    row_index_array(non_zero_a)    = 1_si     ! arbitrary value
    column_index_array(non_zero_a) = 1_si     ! arbitrary value
    ham_elem_array(non_zero_a)     = 0.0_dr   ! set to zero - the only safe value

  end if

  call MPI_Barrier( comm, ierr )

  end subroutine disk_direct_to_ram

end module m_2_disk_direct_to_ram

!========================================================================================
! FINI.
!========================================================================================
