
program driver_fort_i4_to_coo_i4_slow

  use m_1_type_definitions
  use m_2_fort_i4_to_coo_i4_slow

  implicit none

! 1. the input variables for the subroutine:
!    "matrix_fort_to_coo_official_disk_half".

  character(len=200) :: file_name_from_scatci
  character(len=200) :: file_name_coo_i4

!========================================================================================
! 2. defining the input variables for the driver.

  file_name_from_scatci = "fort.26_i4"
  file_name_coo_i4      = "matrix_coo_half_i4"

!========================================================================================

! 3. calling the subroutine.

  call fort_i4_to_coo_i4_slow( file_name_from_scatci,    &
                               file_name_coo_i4 )

end program driver_fort_i4_to_coo_i4_slow
