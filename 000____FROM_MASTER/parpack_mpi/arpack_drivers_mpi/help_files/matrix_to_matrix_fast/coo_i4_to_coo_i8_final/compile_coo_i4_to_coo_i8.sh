#!/bin/bash

# 1. compiling the code

  ifort -O3 -static-intel            \
  m_1_type_definitions.f90           \
  m_2_coo_i4_to_coo_i8.f90           \
  driver_coo_i4_to_coo_i8.f90        \
  -o x_coo_i4_to_coo_i8
 
# 2. cleaning the *.mod produced files.

  rm *.mod

# 3. exiting.
