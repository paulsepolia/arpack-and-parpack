#!/bin/bash

# 1. compiling the code

  ifort -O3 -static-intel                   \
  m_1_type_definitions.f90                  \
  m_2_fort_i4_to_coo_i4.f90                 \
  driver_fort_i4_to_coo_i4.f90              \
  -o x_fo_i4_to_co_i4
 
# 2. cleaning the *.mod produced files.

  rm *.mod

# 3. exiting.
