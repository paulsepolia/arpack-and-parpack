!====================================================================!
!                                                                    !
! author: pavlos g. galiatsatos.                                     !
!                                                                    !
! date: 2010/11/22.                                                  !
!                                                                    !
! infos: this module is a black-box module.                          !
!        you do not need to adjust anything.                         ! 
!====================================================================!

module m_2_fort_i4_to_coo_direct

  use m_1_type_definitions

  implicit none

  contains
  
!===================================================================================================! --> 1.
!===================================================================================================!
     
!=======================================================================================!
!                                                                                       !
! subroutine: "matrix_fort_to_coo_ram_half".                                            !
!                                                                                       !
! a. input:                                                                             !
!                                                                                       !
! character(len=200), intent(in) :: file_name_from_scatci                               !
!                                                                                       !
! b. output:                                                                            !
!                                                                                       !
! integer(kind=si), intent(out) :: dimen_matrix                                         !
! integer(kind=di), intent(out) :: non_zero_ham_elem_half                               !
! integer(kind=si), intent(out), allocatable, dimension(:) :: row_index_array           !
! integer(kind=si), intent(out), allocatable, dimension(:) :: column_index_array        !
! real(kind=dr)   , intent(out), allocatable, dimension(:) :: ham_elem_array            !
!                                                                                       !
! c. purpose:                                                                           !
!                                                                                       !
! writing in ram the indices of rows, columns and the correspoding hamiltonian          !
! matrix elements, the dimension of the matrix and the number of non zero elements      !
! only the upper(lower) triangle.                                                       !
!                                                                                       !
!=======================================================================================!

  subroutine matrix_fort_to_coo_ram_half( file_name_from_scatci,   &
                                          dimen_matrix,            &
                                          non_zero_ham_elem_half,  &
                                          row_index_array,         & 
                                          column_index_array,      & 
                                          ham_elem_array )

  implicit none

! 1.
! a. "dimen_matrix" is the dimension of the hamiltonian matrix.
!    the default type in scatci is single integer precision.
!                                                                                         
! b. "non_zero_ham_elem_half" is the total numbers of non-zero
!    hamiltonian elements of the lower(upper) triangle only.
!    the kind type is double precision  integer, since this variable
!    is getting large values. this fact does not confilct with the
!    kind type definition of the same variable inside scatci,
!    which is single precision integer.
!
! c. these variables have intent(out) attribute, and they are exported
!    to any subroutine which diagonalizes the hamiltonian matrix.

  integer(kind=si), intent(out) :: dimen_matrix
  integer(kind=di), intent(out) :: non_zero_ham_elem_half

! 2. declaration of the arrays for storing in ram the hamiltonian matrix,
!    as a lower(upper) triangular sparse matrix.
!
! a. "row_index_array" is the single integer precision array
!    for storing the numbers of rows which hold the non-zero
!    hamiltonian matrix elements.
!
! b. "column_index_array" is the single integer precision array
!    for storing the numbers of columns which hold the non-zero
!    hamiltonian matrix elements.
!
! c. "ham_elem_array" is the double real precision array
!    for storing the hamiltonian matrix elements.
!
! d. the integer precision of a. and b. is single. with the current kind type
!    we are able to handle a matrix of a dimension 2*(10**9), which is huge.
!    so, there is no reason to alter the kind type to any higher kind type.
!
! e. these variables have intent(out) attribute, and they are exported
!    to any subroutine which diagonalize the hamiltonian matrix.

  integer(kind=si), intent(out), allocatable, dimension(:) :: row_index_array
  integer(kind=si), intent(out), allocatable, dimension(:) :: column_index_array
  real(kind=dr), intent(out), allocatable, dimension(:) :: ham_elem_array

! 3. this variable has intent(in) attribute and is the only input to
!    the "matrix_coo_ram_half" subroutine.

  character(len=200), intent(in) :: file_name_from_scatci

! 4. the input unit.
 
  integer(kind=si), parameter :: matrix_from_scatci = 11

! 5. declaration of some temporary help variables.
!
! a. "i1" is a loop variable.
!
! b. "itmp1" is to store temporarly the buffers. there is no any
!    further process of those values. it is a trash variable.
!
! c. "error_check" is to determine when the reading has
!    reached the end of the "fort.26" file and to exit the
!    infinite reading do-loop. it is a must variable.

  integer(kind=di) :: i1 
  integer(kind=si) :: itmp1, error_check

! 6. declaration of some temporary help arrays.
!
! a. the "ij_tmp" tmp help array is a single precision integer array
!    and must remain as single precision integer array, since the
!    related output of scatci is an integer single precision output.
! 
! b. if the related output of scatci, written in the original
!    scatci file "fort.26", is modified and the single type becomes
!    double integer precision, then i have to modify the "ij_tmp"
!    integer type to double precision integer type.
!
! c. the "emx_tmp" tmp help array is a double precision real array,
!    and must remain as such since the related output of the scatci
!    is a double real precision output.
!    there in no reason this to be changed in the future.
! 
! d. the purpose of the "ij_tmp" array is to store the indices of
!    rows and columns of hamiltonian matrix elememnts.
!    the length of this array is the length of the buffer that the
!    scatci uses to write the "fort.26" file. this buffer length
!    is read and used by this program automatically.
!
! e. the purpose of the "emx_tmp" array is to store the corresponding
!    hamiltonian elements. the length of this array is the length of
!    the buffer that the scatci uses to write the "fort.26" file. this
!    buffer is read and used by this program automatically.
!
! f. these variables are local to the subroutine.
 
  integer(kind=si), allocatable, dimension(:,:) :: ij_tmp
  real(kind=dr), allocatable, dimension(:) :: emx_tmp 
  

! 7.
!
! a. "num_buffer" and "num_buffer_tmp" are variables for the
!    first value buffer and for all the rest, correspondingly.
!
! b. "num_buffer_last" is the dimension of the last buffer.
!
! c. "num_total_buffers" is the total number of buffers inside the "fort.26" file.
!
! d. these variables are local to the subroutine.
 
  integer(kind=si) :: num_buffer
  integer(kind=si) :: num_buffer_tmp
  integer(kind=si) :: num_buffer_last
  integer(kind=si) :: num_total_buffers 

! 8. opening the unit.
!
! a. "matrix_from_scatci" is the unit opened for reading
!    the original "fort.26" scatci produced unformatted file.
 
  open( unit=matrix_from_scatci,     &
        file=file_name_from_scatci,  &
        form="unformatted",          &
        status="old",                &
        action="read" ) 

! 9. reading the dimension of the matrix and the buffer length.
!
! a. first we rewind the "matrix_from_scatci" unit.
!
! b. "dimen_matrix" is the dimension of the hamiltonian matrix.  
!
! c. "num_buffer" is the length of the main buffer.
!    the length of the last buffer will be computed later.
 
  rewind(unit=matrix_from_scatci)
  read(unit=matrix_from_scatci) dimen_matrix, num_buffer 

! 10. evaluating the length of the last buffer and the number of the total buffers.
!
! a. "num_buffer_last" is the length of the last buffer.
!
! b. "num_total_buffers" is the variable which its final value
!    is the total number of buffers inside the "fort.26" file.

  num_total_buffers=0 ! setting to zero.

  do ! do-loop: alpha.

    num_total_buffers=num_total_buffers+1

    read(unit=matrix_from_scatci, iostat=error_check) num_buffer_tmp

    if ((num_buffer_tmp /= num_buffer) .and. (error_check == 0)) then ! if: alpha.

      num_buffer_last=num_buffer_tmp
 
      exit 

    else if (error_check /= 0) then ! else-if: alpha.

      exit

    end if ! end if: alpha.

  end do  ! end of do-loop: alpha.

! 11. evaluating the number of non-zero hamiltonian matrix elements.
!
! a. "non_zero_ham_elem_half" is the number of total upper(lower)
!    triangle non-zero matrix hamiltonian elements.
 
  non_zero_ham_elem_half = (num_total_buffers-1)*num_buffer + num_buffer_last
 
! 12. allocating the temporary arrays.
!
! a. an important note is that the last "ij_tmp" must
!    have the common buffer length size. otherwise the
!    reading "fort.26" process fails. this is because
!    in such a way the "fort.26" is built inside scatci.
!
! b. the following arrays are tmp.
!    they are deallocated as soon as they become useless.

  allocate(ij_tmp(1:2,1:num_buffer)) 
  allocate(emx_tmp(1:num_buffer))    

! 13. allocating the arrays for rows, columns and matrix elements.
!
! a. the following arrays are non-local and have to be passed to
!    any subroutine which diagonalize the matrix. do not deallocate them here.
!    deallocation must be done inside the calling subroutine.

  allocate(row_index_array(1:non_zero_ham_elem_half))
  allocate(column_index_array(1:non_zero_ham_elem_half))
  allocate(ham_elem_array(1:non_zero_ham_elem_half))

! 14. rewinding the "matrix_from_scatci" unit.
!
! a. the purpose is to rewind the unit and to skip the first line.

  rewind(unit=matrix_from_scatci) 
  read(unit=matrix_from_scatci) 

! 15. reading all the buffers except the last and writing the infos.
!
! a. the purpose is to read all the elements up to the previous
!    to the last buffer of "fort.26" file, and on the fly, for
!    each buffer, to store its infos to the non-local arrays
!    "row_index_array", "column_index_array", "ham_elem_array".
!
! b. the reading and writing of the last buffer is the next step.
 
 do i1=1, num_total_buffers-1 ! do-loop: beta.

    read(unit=matrix_from_scatci) itmp1, ij_tmp(:,:), emx_tmp(:)
   
    row_index_array(1+(i1-1)*num_buffer : i1*num_buffer) =     &
                                   
      ij_tmp(1,1:num_buffer)
    
    column_index_array(1+(i1-1)*num_buffer : i1*num_buffer) =  &

      ij_tmp(2,1:num_buffer)
    
    ham_elem_array(1+(i1-1)*num_buffer : i1*num_buffer) =      &
         
      emx_tmp(1:num_buffer)

 end do ! end of do-loop: beta.

! 16. reading the last buffer and writting the infos.
!
! a. the purpose is to read the last buffer and to put the data
!    to the arrays "row_index_array", "column_index_array" and
!    "ham_elem_array".

  read(unit=matrix_from_scatci) itmp1, ij_tmp(:,:), emx_tmp(:)

  row_index_array(num_buffer*(num_total_buffers-1)+1 : non_zero_ham_elem_half) =     &
                             
    ij_tmp(1,1:num_buffer)

  column_index_array(num_buffer*(num_total_buffers-1)+1 : non_zero_ham_elem_half) =  &

    ij_tmp(2,1:num_buffer)

  ham_elem_array(num_buffer*(num_total_buffers-1)+1 : non_zero_ham_elem_half) =      & 
  
    emx_tmp(1:num_buffer)

! 17. deallocating the tmp help arrays.
!
! a. the "ij_tmp(:,:)" and "emx_tmp(:)" arrays are no more needed.

  deallocate(ij_tmp)  
  deallocate(emx_tmp) 

! 18. closing the unit and keeping the file.

  close(unit=matrix_from_scatci, &
        status="keep")

  end subroutine matrix_fort_to_coo_ram_half

!===================================================================================================! --> 2.
!===================================================================================================!

!=======================================================================================!
! subroutine: "matrix_fort_i4_to_coo_direct".                                           !
!                                                                                       !
! a. input:                                                                             !
!                                                                                       !
! character(len=200), intent(in) :: file_name_from_scatci                               !
! character(len=200), intent(in) :: file_name_coo_out                                   !
!                                                                                       !
! b. output:                                                                            !
!                                                                                       !
! integer(kind=si), intent(out) :: dimen_matrix                                         !
! integer(kind=di), intent(out) :: non_zero_ham_elem                                    !
!                                                                                       !
! c. purpose:                                                                           !
!                                                                                       !
! the hamiltonian matrix to be written to the hard disk in coordinate sparse            ! 
! format. the execution is fast but i need the same amount of ram                       !
! as the whole matrix occupies in disk. only the lower(upper) triangle.                 !
!                                                                                       ! 
!=======================================================================================!
  
  subroutine matrix_fort_i4_to_coo_direct ( file_name_from_scatci,    &
                                            file_name_coo_out )

  implicit none

! 1. the names of the files to be read and written.

  character(len=200), intent(in) :: file_name_from_scatci
  character(len=200), intent(in) :: file_name_coo_out

! 2. declaration of the constant variables of the code.
!
! a. "dimen_matrix" is the dimension of the hamiltonian matrix.
!    the default type in scatci is single integer precision.
!
! b. "non_zero_ham_elem_half" is the total numbers of non-zero
!    hamiltonian elements lower(upper) triangle.
!    the kind type is double precision integer,
!    since this variable is getting large values.
!    this fact does not confilct with the kind type definition
!    of the same variable inside scatci, which is single precision integer.
!
! c. these variables have intent(out) attribute, and they are exported
!    to any subroutine which diagonalizes the hamiltonian matrix.

  integer(kind=si) :: dimen_matrix
  integer(kind=di) :: non_zero_ham_elem_half

! 3. the unit to be opened.
!
! a. "matrix_coo_out" is the unit for the new in the coordinate format
!    written to hard disk.

  integer(kind=si), parameter :: matrix_from_scatci = 100
  integer(kind=si), parameter :: matrix_coo_out = 200
                                                                                       
! 4. some tmp variables.
!
! a. "row_index_array" is the tmp array for the rows indices.
!
! b. "column_index_array" is the tmp array for the columns indices.
!
! c. "ham_elem_array" is the tmp array for the hamiltonian matrix elements.
!
! d.  "i1" is a loop variable.

  integer(kind=si), allocatable, dimension(:) :: row_index_array
  integer(kind=si), allocatable, dimension(:) :: column_index_array
  real(kind=dr), allocatable, dimension(:) :: ham_elem_array
  integer(kind=di) :: i1
  integer(kind=si) :: rec_len
  real(kind=dr), dimension(3) :: dimen_line 

! 5.

  call  matrix_fort_to_coo_ram_half( file_name_from_scatci,   &
                                     dimen_matrix,            &
                                     non_zero_ham_elem_half,  &
                                     row_index_array,         &
                                     column_index_array,      &
                                     ham_elem_array )

! 6. opening the unit for writing.

  inquire( iolength=rec_len ) dimen_line ! evaluating the record length

  open(unit=matrix_coo_out,     &
       file=file_name_coo_out,  &
       form="unformatted",      &
       status="new",            &
       action="write",          &
       access="direct",         &
       recl=rec_len)

! 7. writing to the hard disk.
    
  ! 7.a. writing the head of the "coo_matrix_direct_i8" file.

    rewind( unit = matrix_coo_out )
    write( unit=matrix_coo_out, rec=1 )  real( dimen_matrix, kind=dr ),           &
                                         real( dimen_matrix, kind=dr ),           &
                                         real( non_zero_ham_elem_half, kind=dr ) 

  ! 7.b writing the rest of the "coo_matirx_direct_i8" file 

    do i1 = 1, non_zero_ham_elem_half ! do-loop: alpha.
 
      write(unit=matrix_coo_out,rec=i1+1)  real( row_index_array(i1), kind=dr ),       &
                                           real( column_index_array(i1), kind=dr ),    &
                                           ham_elem_array(i1)

    end do ! end of do-loop: alpha.

! 8. closing the unit and keeping the written file.

  close( unit=matrix_coo_out, status="keep" )

! 9. deallocating 

  deallocate( row_index_array )
  deallocate( column_index_array )
  deallocate( ham_elem_array )

  end subroutine matrix_fort_i4_to_coo_direct

!===================================================================================================! --> 3.
!===================================================================================================!

end module m_2_fort_i4_to_coo_direct
