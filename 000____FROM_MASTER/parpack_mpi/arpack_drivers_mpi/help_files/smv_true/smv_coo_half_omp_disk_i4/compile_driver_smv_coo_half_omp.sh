#!/bin/bash

# part alpha.

 ifort -O3 -static-intel                  \
 -parallel -par-threshold0                \
 -par-report1                             \
 -openmp                                  \
 -openmp-report2                          \
 -mkl=parallel                            \
 m_1_type_definitions.f90                 \
 m_2_coo_half_to_ram.f90                  \
 m_3_smv_coo_half_omp.f90                 \
 driver_smv_coo_half_omp.f90              \
 -o x_smv_omp

# part beta.

 rm *.mod

# exiting.

 exit
