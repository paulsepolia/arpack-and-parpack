#!/bin/bash

# 1. compiling the code

  ifort -O3 -static-intel                          \
  -parallel -par-threshold0                        \
  -par-report1                                     \
  -mkl=parallel                                    \
  -openmp                                          \
  -openmp-report2                                  \
  m_1_type_definitions.f90                         \
  m_2_matrix_coo_half_disk_direct_to_ram.f90       \
  m_3_smv_coo_half_omp_8_disk_direct.f90           \
  driver_smv_coo_half_omp_8_disk_direct.f90        \
  -o x_smv_omp_disk_direct
 
# 2. cleaning the *.mod produced files.

  rm *.mod

# 3. exiting.
