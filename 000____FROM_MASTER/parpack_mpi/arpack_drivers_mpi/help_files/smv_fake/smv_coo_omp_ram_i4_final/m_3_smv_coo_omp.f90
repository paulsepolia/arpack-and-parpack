!====================================================================!
!                                                                    !
! Author: Pavlos G. Galiatsatos.                                     !
!                                                                    !
! Date: 2011/07/16.                                                  !
!                                                                    !
!====================================================================!

module m_3_smv_coo_omp

  use m_1_type_definitions

  implicit none

  contains

  subroutine smv_coo_omp(  row_index_array,      &  
                           column_index_array,   &  
                           ham_elem_array,       &  
                           matrix_dimen,         &     
                           non_zero_ham_elem,    &
                           x,                    &
                           y )
              

  implicit none

! 1. variables.

  integer(kind=si), intent(in),  allocatable, dimension(:) :: row_index_array
  integer(kind=si), intent(in),  allocatable, dimension(:) :: column_index_array
  real(kind=dr),    intent(in),  allocatable, dimension(:) :: ham_elem_array         
  real(kind=dr),    intent(in),  allocatable, dimension(:) :: x
  real(kind=dr),    intent(out), allocatable, dimension(:) :: y
  integer(kind=si), intent(in)                             :: matrix_dimen
  integer(kind=di), intent(in)                             :: non_zero_ham_elem

! 2. openmp help variables. openmp part 1.

  integer(kind=si) :: nt, omp_get_num_threads
  real(kind=dr), allocatable, dimension(:,:) ::  x1
  real(kind=dr), allocatable, dimension(:,:) ::  y1
  integer(kind=di) :: i2
  integer(kind=si) :: i1
  integer(kind=si), allocatable ,dimension(:) :: irow, icolumn

!$omp parallel

  nt = omp_get_num_threads()

!$omp end parallel

  allocate( y(1:matrix_dimen) )  

  allocate( x1( 1:matrix_dimen, 1:nt ) )      ! x1
  allocate( y1( 1:matrix_dimen, 1:nt ) )      ! y1
  allocate( irow(1:nt) )                    ! irow
  allocate( icolumn(1:nt) )                 ! icolumn
  
    do i1 = 1, nt
      x1(:,i1) = x        !  1.
    end do

    y1 = 0.0d0


!==============================================================================
!******************************************************************************
! omp_auto do-loop.

!$OMP PARALLEL DO                     &
!$OMP DEFAULT( NONE )                 &
!$OMP PRIVATE( i1 )                   & 
!$OMP PRIVATE( i2 )                   &
!$OMP SHARED( non_zero_ham_elem )     &
!$OMP SHARED( nt )                    &
!$OMP SHARED( irow )                  &
!$OMP SHARED( icolumn )               &
!$OMP SHARED( y1 )                    &
!$OMP SHARED( x1 )                    &
!$OMP SHARED( ham_elem_array )        &
!$OMP SHARED( row_index_array )       &
!$OMP SHARED( column_index_array )

    do i1 = 1, nt

      do i2 = (i1-1) * non_zero_ham_elem / nt + 1 , (i1) * non_zero_ham_elem / nt     
 
        irow(i1) = row_index_array(i2)
 
        icolumn(i1) = column_index_array(i2)
 
        y1(irow(i1),i1) = y1(irow(i1),i1) + ham_elem_array(i2) * x1(icolumn(i1),i1)

      end do
 
    end do 

!$OMP END PARALLEL DO

    !==================================================================
    !# WARNING : DANGER OF ROUND-OFF ERRORS AT THE FOLLOWING STATEMENT.

    y = 0.0d0

    do i1 = 1, nt

      y = y + y1(:,i1) 

    end do      
  
    !==================================================================

  end subroutine smv_coo_omp

end module m_3_smv_coo_omp
