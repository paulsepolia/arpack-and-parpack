#!/bin/bash

# 1. compiling the code

  ifort -O3 -static-intel                          \
  -parallel -par-threshold0                        \
  -par-report1                                     \
  -openmp                                          \
  -openmp-report2                                  \
  m_1_type_definitions.f90                         \
  m_2_coo_ram.f90                                  \
  m_3_smv_coo_omp.f90                              \
  driver_smv_coo_omp_ram.f90                       \
  -o x_smv_coo_omp
 
# 2. cleaning the *.mod produced files.

  rm *.mod

# 3. exiting.
