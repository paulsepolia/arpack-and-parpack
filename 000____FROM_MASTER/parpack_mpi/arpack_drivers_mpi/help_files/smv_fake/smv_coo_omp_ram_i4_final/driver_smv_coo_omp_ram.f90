
program driver_smv_coo_omp_ram

  use m_1_type_definitions
  use m_2_coo_ram
  use m_3_smv_coo_omp

  implicit none

! 1. declarations of the intreface variables.

  integer(kind=si), parameter :: matrix_dimen_sparse = 300000
  integer(kind=si), parameter :: trials = 5000
  real(kind=dr), parameter    :: sparsity = 99.78_dr

! 2. local variables.

  integer(kind=di) :: non_zero_ham_elem
  integer(kind=si), allocatable, dimension(:) :: row_index_array
  integer(kind=si), allocatable, dimension(:) :: column_index_array
  real(kind=dr)   , allocatable, dimension(:) :: ham_elem_array 
  real(kind=dr)   , allocatable, dimension(:) :: x 
  real(kind=dr)   , allocatable, dimension(:) :: y 
  real(kind=dr) :: t1, t2  
  integer(kind=si) :: i
  integer(kind=di) :: non_zero_ham_elem_help
  integer(kind=si) :: matrix_dimen_dense
  integer(kind=si) :: nt
  integer(kind=si) :: omp_get_num_threads

! 3. calling the sub which reads the coo_sparse matrix.

!$omp parallel

  nt = omp_get_num_threads()

!$omp end parallel

  call matrix_coo_ram( matrix_dimen_sparse,        &
                       non_zero_ham_elem,          &
                       row_index_array,            &
                       column_index_array,         &
                       ham_elem_array,             &
                       sparsity )


  write(*,*) "==========================================================================="
  write(*,*) "  1. driver program."
  write(*,*) "==========================================================================="
  write(*,*) 
  write(*,*) "  2. matrix_dimen_sparse         --> ", matrix_dimen_sparse
  write(*,*) "  3. non_zero_ham_elem           --> ", non_zero_ham_elem
  write(*,*) "  4. sparsity                    --> ", sparsity
  write(*,*) "  5. row_index_array(1)          --> ", row_index_array(1)
  write(*,*) "  6. column_index_array(1)       --> ", column_index_array(1)
  write(*,*) "  7. ham_elem_array(1)           --> ", ham_elem_array(1)

! 4. initializing the x vector.

  allocate( x(1:matrix_dimen_sparse) ) 

  do i = 1, matrix_dimen_sparse

    x(i) = dcos(dcos( real(i,kind=dr) ))

  end do

! 5. calling the sub sparse matrix vector product.

  call cpu_time(t1)

  do i = 1, trials

    call smv_coo_omp( row_index_array,      & 
                      column_index_array,   &
                      ham_elem_array,       &
                      matrix_dimen_sparse,  &
                      non_zero_ham_elem,    &
                      x,                    &
                      y )    

  end do

  call cpu_time(t2)

! 6. writing out for testing purposes.

  non_zero_ham_elem_help = (real(matrix_dimen_sparse,kind=dr)**2) * (100-sparsity) / 100
  matrix_dimen_dense = dsqrt( real( non_zero_ham_elem_help, kind=dr) )
  matrix_dimen_dense = matrix_dimen_dense + mod(matrix_dimen_dense,nt)
  
  write(*,*) "  8. number of smv products done --> ", trials
  write(*,*) "  9. total real time (seconds)   --> ", (t2-t1)/nt
  write(*,*) " 10. smv products per second     --> ", trials * nt / (t2-t1)
  write(*,*) " 11. some results are: "
  write(*,*) ""

  do i = 1, 2 * (matrix_dimen_dense/nt)

    write(*,*)  i, x(i), y(i)

  end do

! 7. deallocations.

  deallocate(x)
  deallocate(y)
  deallocate(row_index_array)
  deallocate(column_index_array)
  deallocate(ham_elem_array)

end program driver_smv_coo_omp_ram
