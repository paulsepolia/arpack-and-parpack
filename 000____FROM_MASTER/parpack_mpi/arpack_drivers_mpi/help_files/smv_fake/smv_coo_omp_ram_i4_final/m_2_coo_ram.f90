!====================================================================!
!                                                                    !
! author: pavlos g. galiatsatos.                                     !
!                                                                    ! 
! date: 2010/11/25.                                                  !
!                                                                    !
!====================================================================!

module m_2_coo_ram

  use m_1_type_definitions

  implicit none

  contains

  subroutine matrix_coo_ram( matrix_dimen_sparse,      &
                             non_zero_ham_elem,        &
                             row_index_array,          &            
                             column_index_array,       &
                             ham_elem_array,           &
                             sparsity )
                                    

  implicit none

  ! 1. interface variables.

  integer(kind=si), dimension(:), allocatable, intent(out) :: row_index_array
  integer(kind=si), dimension(:), allocatable, intent(out) :: column_index_array
  real(kind=dr)   , dimension(:), allocatable, intent(out) :: ham_elem_array
  integer(kind=si), intent(in) :: matrix_dimen_sparse
  integer(kind=di), intent(out) :: non_zero_ham_elem
  real(kind=dr), intent(in) :: sparsity 

  ! 2. local variables.
  
  integer(kind=si) :: nt 
  integer(kind=si) :: omp_get_num_threads
  integer(kind=si) :: matrix_dimen_dense
  integer(kind=di) :: non_zero_ham_elem_help
  integer(kind=di) :: i1, i2

  ! 3. allocating ram space.

!$omp parallel

  nt =omp_get_num_threads()

!$omp end parallel

  non_zero_ham_elem_help = (real(matrix_dimen_sparse,kind=dr)**2) * (100-sparsity) / 100
  matrix_dimen_dense = dsqrt( real( non_zero_ham_elem_help, kind=dr) )
  matrix_dimen_dense = matrix_dimen_dense + mod( matrix_dimen_dense, nt )
  non_zero_ham_elem = real( matrix_dimen_dense, kind=dr ) * real ( matrix_dimen_dense, kind=dr )

  allocate( row_index_array     (1:non_zero_ham_elem) )
  allocate( column_index_array  (1:non_zero_ham_elem) )
  allocate( ham_elem_array      (1:non_zero_ham_elem) )
 
  ! 4. building the matrix. 
  !    the matrix can be real symmetric or non-symmetric.

  do i1 = 1, matrix_dimen_dense
    do i2 = 1, matrix_dimen_dense

      ham_elem_array( i2 + (i1-1) * matrix_dimen_dense ) = dcos( real(i1*i2, kind=dr) )

    end do
  end do

  ! 5. building the row_index_array & column_index_array

  do i1 = 1, matrix_dimen_dense
    do i2 = 1, matrix_dimen_dense

      row_index_array( i2 + (i1-1) * matrix_dimen_dense ) = i1
      column_index_array( i2 + (i1-1) * matrix_dimen_dense ) = i2     

    end do 
  end do

  end subroutine matrix_coo_ram

end module m_2_coo_ram
