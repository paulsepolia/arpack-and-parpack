#!/bin/bash

  # alpha.

  mpiifort -O3 -static_mpi    \
  -static-intel               \
  m_1_type_definitions.f90    \
  smv_coo_mpi_ram_i4.f90      \
  -o x_smv_mpi_ram_i4

  # beta.

  rm *.mod

  # gamma.
