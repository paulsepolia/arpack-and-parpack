!====================================================================!
!                                                                    !
! author: pavlos g. galiatsatos.                                     !
!                                                                    !
! date: 2010/04/12.                                                  !
!                                                                    !
!====================================================================!

  module m_3_smv_coo_half_mpi
 
  use mpi
  use m_1_type_definitions

  implicit none

  contains 

!========================================================================================
! subroutine: smv_coo_half_mpi
!========================================================================================

  subroutine smv_coo_half_mpi( comm,                     &    !  1.
                               my_rank,                  &    !  2.
                               p,                        &    !  3.
                               matrix_dimen,             &    !  4.
                               non_zero_ham_elem,        &    !  5.
                               row_index_array,          &    !  6.
                               column_index_array,       &    !  7.
                               ham_elem_array,           &    !  8.
                               non_zero_local,           &    !  9.
                               x,                        &    ! 10.
                               y,                        &    ! 11.
                               y_final )                      ! 12.

  ! 1. declarations of the interface variables.

  integer(kind=si), intent(in)                                :: comm
  integer(kind=si), intent(in)                                :: my_rank                !  2.
  integer(kind=si), intent(in)                                :: p                      !  3.
  integer(kind=si), intent(in)                                :: matrix_dimen           !  4.
  integer(kind=di), intent(in)                                :: non_zero_ham_elem      !  5.
  integer(kind=si), intent(in),    allocatable, dimension(:)  :: row_index_array        !  6.
  integer(kind=si), intent(in),    allocatable, dimension(:)  :: column_index_array     !  7.
  real(kind=dr)   , intent(in),    allocatable, dimension(:)  :: ham_elem_array         !  8.
  integer(kind=di), intent(in)                                :: non_zero_local         !  9.
  real(kind=dr),    intent(in),    allocatable, dimension(:)  :: x                      ! 10.
  real(kind=dr),    intent(inout), allocatable, dimension(:)  :: y                      ! 11.
  real(kind=dr),    intent(inout), allocatable, dimension(:)  :: y_final                ! 12.

  ! 2. declarations of the rest variables.

  integer(kind=di)  :: i
  integer(kind=si)  :: ierr
  integer(kind=si)  :: irow
  integer(kind=si)  :: icolumn

  ! 3. broadcasting the x vector to the rest of 

  call MPI_Bcast( x, matrix_dimen, MPI_DOUBLE_PRECISION, 0, comm, ierr )
  call MPI_Barrier( comm, ierr )

  ! 4. the mpi sparse matrix - vector product 

  ! 4.a.
  y = 0.0_dr

  ! 4.b. 
  do i = 1, non_zero_local
  
    irow = row_index_array(i)
  
    icolumn = column_index_array(i)
  
    y(irow) = y(irow) + ham_elem_array(i) * x(icolumn)
  
!    if (irow.ne.icolumn) y(icolumn) = y(icolumn) + ham_elem_array(i) * x(irow)
  
  end do 

  call MPI_Barrier( comm, ierr ) 

  ! 4.c.
  call MPI_Reduce( y,                        &
                   y_final,                  &
                   matrix_dimen,             &
                   MPI_DOUBLE_PRECISION,     & 
                   MPI_SUM,                  & 
                   0,                        &
                   comm,                     &
                   ierr )


  call MPI_Barrier( comm, ierr )

  end subroutine smv_coo_half_mpi

!========================================================================================

  end module m_3_smv_coo_half_mpi

!========================================================================================
! FINI.
!========================================================================================
