!====================================================================!
!                                                                    !
! author: pavlos g. galiatsatos.                                     !
!                                                                    !
! date: 2010/04/12.                                                  !
!                                                                    !
!====================================================================!

  module m_2_matrix_coo_ram

  use mpi
  use m_1_type_definitions

  contains

!========================================================================================
! 1. subroutine: matrix_coo_ram.
!========================================================================================
  
  subroutine matrix_coo_ram( comm,                     & 
                             my_rank,                  &
                             p,                        &
                             matrix_dimen,             &
                             row_index_array,          &
                             column_index_array,       &
                             ham_elem_array,           &
                             non_zero_ham_elem,        &
                             non_zero_local ) 

  implicit none 

  ! 1. interface variables.

  integer(kind=si), intent(in)                                :: comm
  integer(kind=si), intent(in)                                :: my_rank
  integer(kind=si), intent(in)                                :: p
  integer(kind=si), intent(in)                                :: matrix_dimen
  integer(kind=si), intent(inout), allocatable, dimension(:)  :: row_index_array
  integer(kind=si), intent(inout), allocatable, dimension(:)  :: column_index_array
  real(kind=dr),    intent(inout), allocatable, dimension(:)  :: ham_elem_array
  integer(kind=di), intent(in)                                :: non_zero_ham_elem
  integer(kind=di), intent(in)                                :: non_zero_local

  ! 2. local variables.

  integer(kind=di)  :: i1
  integer(kind=di)  :: i2
  integer(kind=si)  :: ierr

  ! 3.a. building the local matrices.

  do i1 = 1, matrix_dimen / p
    do i2 = 1, matrix_dimen

      ham_elem_array(i2+(i1-1)*matrix_dimen) =                     & 
      dcos(real((i1+my_rank*matrix_dimen/p)*i2, kind=dr))

    end do
  end do

  ! 3.b. building the local matrices.

  do i1 = 1, matrix_dimen / p
    do i2 = 1, matrix_dimen

      row_index_array(i2+(i1-1)*matrix_dimen)    = int(i1+my_rank*(matrix_dimen/p), kind=si)
      column_index_array(i2+(i1-1)*matrix_dimen) = int(i2, kind=si )     

    end do 
  end do

  call MPI_Barrier( comm, ierr )

  ! 4. finishing.

  end subroutine matrix_coo_ram

  end module m_2_matrix_coo_ram  

!========================================================================================
! FINI.
!========================================================================================

