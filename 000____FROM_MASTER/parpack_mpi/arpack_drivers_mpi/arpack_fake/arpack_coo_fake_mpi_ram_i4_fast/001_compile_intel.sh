#!/bin/bash

# 1. compiling

	mpiifort  -O3                           \
			  -static-intel                 \
  			  -mkl=cluster                  \
  			  m_1_type_definitions.f90      \
  			  m_2_matrix_coo_ram.f90        \
  			  m_3_smv_coo_half_mpi.f90      \
  			  driver_arpack_coo_mpi_ram.f90 \
  			  /opt/parpack/lib_2015/libparpack_intel_mkl.a  \
  			  /opt/parpack/lib_2015/libarpack_intel_mkl.a   \
  			  -o x_intel

# 2. cleaning 

  rm *.mod
