!====================================================================!
!                                                                    !
! Author: Pavlos G. Galiatsatos.                                     !
!                                                                    !
! Date: 2011/07/26.                                                  !
!                                                                    !
!====================================================================!

  program driver_arpack_coo_mpi 

  use mpi
  use m_1_type_definitions
  use m_2_matrix_coo_ram
  use m_3_smv_coo_half_mpi

  implicit none

!==================================================================================================  
! 1. interface of the driver program. 
!==================================================================================================

  integer(kind=si),   parameter :: n         = 1000          ! dimension of the matrix
  integer(kind=si),   parameter :: nev       = 50            ! number of eigenvalues to get back
  integer(kind=si),   parameter :: ncv_coeff = 2             ! the size of arnoldi space
  real(kind=dr)                 :: tol       = 0.0_dr        ! tolerance
  character(len=200), parameter :: va_file   = "___va_mpi"   ! file name for eigenvalues
  character(len=200), parameter :: ve_file   = "___ve_mpi"   ! file name for eigenvectors
  character(len=1)              :: bmat      = 'I'           ! normal problem
  character(len=2)              :: which     = 'SA'          ! the part of the spectrum

!==================================================================================================
! DO NOT MODIFY ANYTHING BELOW THIS LINE.
!==================================================================================================

  ! 2. declaration of any other parameter and variable.

  integer(kind=si), parameter                   :: maxitr    = 3000000
  integer(kind=si), parameter                   :: ncv = ncv_coeff * nev + 1
  integer(kind=si), parameter                   :: maxnloc =  n + 100
  integer(kind=si)                              :: maxnev = nev + 10
  integer(kind=si)                              :: maxncv = ncv + 10
  integer(kind=si)                              :: ldv = maxnloc
  integer(kind=si)                              :: comm
  integer(kind=si)                              :: nloc
  real(kind=dr),    allocatable, dimension(:)   :: workl
  real(kind=dr),    allocatable, dimension(:)   :: workd
  real(kind=dr),    allocatable, dimension(:)   :: resid
  logical,          allocatable, dimension(:)   :: select_my
  integer(kind=si), allocatable, dimension(:)   :: iparam
  integer(kind=si), allocatable, dimension(:)   :: ipntr
  integer(kind=si)                              :: ido
  integer(kind=si)                              :: lworkl
  integer(kind=si)                              :: info
  integer(kind=si)                              :: ierr
  integer(kind=si)                              :: j
  integer(kind=si)                              :: ndigit
  integer(kind=si)                              :: logfil
  integer(kind=si)                              :: nconv
  integer(kind=si)                              :: mode
  integer(kind=si)                              :: ishfts
  integer(kind=si)                              :: msaupd
  logical                                       :: rvec
  real(kind=dr)                                 :: sigma
  real(kind=dr)                                 :: pdnorm2 
  real(kind=dr),    allocatable, dimension(:,:) :: vectors_array
  real(kind=dr),    allocatable, dimension(:,:) :: values_array
  integer(kind=si), parameter                   :: matrix_dimen = n 
  integer(kind=si)                              :: my_rank
  integer(kind=si)                              :: p
  integer(kind=si), allocatable, dimension(:)   :: row_index_array
  integer(kind=si), allocatable, dimension(:)   :: column_index_array
  real(kind=dr),    allocatable, dimension(:)   :: ham_elem_array
  integer(kind=si)                              :: i1
  integer(kind=si)                              :: i2
  integer(kind=si)                              :: i3
  integer(kind=si)                              :: irow
  integer(kind=si)                              :: icolumn
  real(kind=dr), allocatable, dimension(:)      :: x
  real(kind=dr), allocatable, dimension(:)      :: y
  real(kind=dr), allocatable, dimension(:)      :: y_final
  real(kind=dr)                                 :: t1_mpi
  real(kind=dr)                                 :: t2_mpi
  real(kind=dr)                                 :: t1
  real(kind=dr)                                 :: t2
  integer(kind=di)                              :: non_zero_ham_elem
  integer(kind=di)                              :: non_zero_local
  real(kind=dr)                                 :: total_time_pdsaupd
  real(kind=dr)                                 :: total_time_pdseupd
  real(kind=dr)                                 :: total_time_mv
  real(kind=dr)                                 :: total_time_pdsaupd_mpi
  real(kind=dr)                                 :: total_time_pdseupd_mpi
  real(kind=dr)                                 :: total_time_mv_mpi
  real(kind=dr), allocatable, dimension(:)      :: x_help
  integer(kind=di)                              :: rec_len
  real(kind=dr)                                 :: help_var
  integer(kind=si)                              :: error_check
  integer(kind=si), dimension(:), allocatable   :: array_loc
  integer(kind=si), parameter                   :: a_para = 2000        
  character(4)                                  :: tmp_file_a
  character(9)                                  :: tmp_file_b
  integer(kind=si)                              :: n_new

  ! 3. starting mpi. 

  call MPI_Init( ierr )

  comm = MPI_COMM_WORLD

  call MPI_Comm_rank( comm, my_rank, ierr )

  call MPI_Comm_size( comm, p, ierr )

  ! 4. the allocations.

  allocate ( workl(1:maxncv*(maxncv+8)) )
  allocate ( workd(1:3*maxnloc) )
  allocate ( resid(1:maxnloc) )
  allocate ( select_my(1:maxncv) )
  allocate ( iparam(1:11) )
  allocate ( ipntr(1:11) )  
  allocate ( vectors_array(1:ldv,maxncv) )
  allocate ( values_array(maxncv,2) )
  
  ! 5. parpack definitions.

  ndigit = -3
  logfil =  6
  msaupd =  1
  nloc   =  n/p

  ! 6. error checks.

  if ( nloc > maxnloc ) then

    write(*,*), " ERROR with _SDRV1: NLOC is greater than MAXNLOC "
    go to 9000

  else if ( nev > maxnev ) then

    write(*,*), " ERROR with _SDRV1: NEV is greater than MAXNEV "
    go to 9000

  else if ( ncv > maxncv ) then

    write(*,*), " ERROR with _SDRV1: NCV is greater than MAXNCV "
    go to 9000

  end if

  ! 7. zeroed timing variables.

  total_time_mv          = 0.0_dr 
  total_time_mv_mpi      = 0.0_dr
  total_time_pdsaupd     = 0.0_dr
  total_time_pdsaupd_mpi = 0.0_dr
  total_time_pdseupd     = 0.0_dr
  total_time_pdseupd_mpi = 0.0_dr

  ! 8. parpack definitions.

  lworkl    = ncv*(ncv+8)
  info      = 0
  ido       = 0
  ishfts    = 1
  mode      = 1
  iparam(1) = ishfts 
  iparam(3) = maxitr 
  iparam(7) = mode 

  ! 9. about the dense matrix.

  non_zero_ham_elem = real(matrix_dimen,kind=dr) * real(matrix_dimen,kind=dr)

  ! 10.  allocating space for the local arrays
  !      to put the sparse matrix data in each mpi thread.

  non_zero_local = non_zero_ham_elem / p

  allocate( row_index_array(1:non_zero_local) )
  allocate( column_index_array(1:non_zero_local) )
  allocate( ham_elem_array(1:non_zero_local) )

  ! 11. building the matrix. 

  call matrix_coo_ram( comm,                     & 
                       my_rank,                  &
                       p,                        &
                       matrix_dimen,             &
                       row_index_array,          &
                       column_index_array,       &
                       ham_elem_array,           &
                       non_zero_ham_elem,        &
                       non_zero_local ) 

  ! 12. main lanczos loop.

  ! 12.a. some allocations. 

  allocate( x_help(1:matrix_dimen) )
  allocate( x(1:matrix_dimen) )
  allocate( y(1:matrix_dimen) )
  allocate( y_final(1:matrix_dimen) )

  ! 12.b. main loop. the "biggest goto" loop.

  10 continue

  if ( my_rank == 0 ) then
    call cpu_time(t1)
  end if
  t1_mpi = MPI_Wtime()

  call pdsaupd ( comm,            &   !  0.
                 ido,             &   !  1.
                 bmat,            &   !  2.
                 nloc,            &   !  3. -->  local matrix dimen.
                 which,           &   !  4.
                 nev,             &   !  5.
                 tol,             &   !  6.
                 resid,           &   !  7.
                 ncv,             &   !  8.
                 vectors_array,   &   !  9.
                 ldv,             &   ! 10.
                 iparam,          &   ! 11.
                 ipntr,           &   ! 12.
                 workd,           &   ! 13.
                 workl,           &   ! 14.
                 lworkl,          &   ! 15.
                 info )               ! 16.

  if ( my_rank == 0 ) then
    call cpu_time(t2)
  end if
  t2_mpi = MPI_Wtime()

  total_time_pdsaupd     = total_time_pdsaupd + (t2-t1)             ! timing pdsaupd.
  total_time_pdsaupd_mpi = total_time_pdsaupd_mpi + (t2_mpi-t1_mpi) ! timing pdsaupd_mpi.

  if (ido == -1 .or. ido == 1) then  ! the "biggest if".
          
  ! 12.c. building the x vector 
 
    x_help = 0.0_dr
    x = 0.0_dr 

    x_help(my_rank*nloc+1:(my_rank+1)*nloc) = workd(ipntr(1):ipntr(1)+nloc)

    call MPI_Barrier( comm, ierr )

    ! 12.d. broadcasting the x vector to the rest of the mpi theads

    if ( my_rank == 0 ) then
      call cpu_time(t1)
    end if
    t1_mpi = MPI_Wtime()

    call MPI_Reduce( x_help,                  &
                     x,                       &
                     matrix_dimen,            &
                     MPI_DOUBLE_PRECISION,    &               
                     MPI_SUM,                 &
                     0,                       &
                     MPI_COMM_WORLD,          &
                     ierr )

    call MPI_Barrier( comm, ierr )

   ! 13. the mpi sparse matrix - vector product 

   y = 0.0_dr

   call smv_coo_half_mpi( comm,                     &    !  1.
                          my_rank,                  &    !  2.
                          p,                        &    !  3.
                          matrix_dimen,             &    !  4.
                          non_zero_ham_elem,        &    !  5.
                          row_index_array,          &    !  6.
                          column_index_array,       &    !  7.
                          ham_elem_array,           &    !  8.
                          non_zero_local,           &    !  9.
                          x,                        &    ! 10.
                          y,                        &    ! 11.
                          y_final )                      ! 12.

    ! 14. the result.

    call MPI_Bcast( y_final, matrix_dimen, MPI_DOUBLE_PRECISION, 0, comm, ierr )

    call MPI_Barrier( comm, ierr )

    workd(ipntr(2):ipntr(2)+nloc) = y_final(my_rank*nloc+1:(my_rank+1)*nloc)

    if ( my_rank == 0 ) then
      call cpu_time(t2)
    end if
    t2_mpi = MPI_Wtime()

    total_time_mv     = total_time_mv + (t2-t1)             ! timing mv.
    total_time_mv_mpi = total_time_mv_mpi + (t2_mpi-t1_mpi) ! timing mv_mpi.

    ! 15. loop again if necessary.
    go to 10
  
  end if ! end of the "biggest if".

  ! 16. some checks and building the vectors.

  ! 16.a. error checking.

  if ( info < 0 ) then

    if ( my_rank == 0 ) then
            
      write(*,*), " "
      write(*,*), " Error with _saupd, info = ", info
      write(*,*), " Check documentation in _saupd "
      write(*,*), " "
    
    endif

  else 
  
  ! 16.b. production of the eigenpairs. 

    rvec = .true.

    if ( my_rank == 0 ) then
      call cpu_time(t1)
    end if
    t1_mpi = MPI_Wtime()

    call pdseupd ( comm,           &     !  0.
                   rvec,           &     !  1.
                   'All',          &     !  2.
                   select_my,      &     !  3.
                   values_array,   &     !  4.
                   vectors_array,  &     !  5.
                   ldv,            &     !  6.
                   sigma,          &     !  7.
                   bmat,           &     !  8.
                   nloc,           &     !  9.
                   which,          &     ! 10.
                   nev,            &     ! 11.
                   tol,            &     ! 12.
                   resid,          &     ! 13.
                   ncv,            &     ! 14.
                   vectors_array,  &     ! 15.
                   ldv,            &     ! 16.
                   iparam,         &     ! 17.
                   ipntr,          &     ! 18.
                   workd,          &     ! 19.
                   workl,          &     ! 20.
                   lworkl,         &     ! 21.
                   ierr )                ! 22.

    if ( my_rank == 0 ) then
      call cpu_time(t2)
    end if
    t2_mpi = MPI_Wtime()

    total_time_pdseupd     = total_time_pdseupd + (t2-t1)             ! timing pdseupd.
    total_time_pdseupd_mpi = total_time_pdseupd_mpi + (t2_mpi-t1_mpi) ! timing pdseupd_mpi.

    ! 16.c. further error checks.
                   
    if ( ierr /= 0) then
      if ( my_rank == 0 ) then

        write(*,*), " "
        write(*,*), " Error with _seupd, info = ", ierr
        write(*,*), " Check the documentation of _seupd. "
        write(*,*), " "

      end if
    end if

    ! 16.d. outputs. 

    if ( ierr == 0 .and. my_rank == 0 ) then
             
      write(*,*) "==========================================================================="
      write(*,*) " Inside driver program --> 1. "
      write(*,*) 
      write(*,*) " Some characteristics values of the matrix under diagonalization."
      write(*,*) 
      write(*,*) " matrix_dimen --> ", matrix_dimen
      write(*,*) " non_zero_ham_elem --> ", non_zero_ham_elem
      write(*,*) " row_index_array(100) --> ", row_index_array(100)
      write(*,*) " column_index_array(100) --> ", column_index_array(100)
      write(*,*) " ham_elem_array(100) --> ", ham_elem_array(100)
      write(*,*) 
      write(*,*) "==========================================================================="
      write(*,*)

      write(*,*) "==========================================================================="
      write(*,*) " Inside driver program --> 2. "
      write(*,*) 
      write(*,*) " total_time_matrix_vector_products (seconds) --> ", total_time_mv
      write(*,*) " total_time_pdsaupd_subroutine (seconds) --> ", total_time_pdsaupd
      write(*,*) " total_time_pdseupd (seconds) --> ", total_time_pdseupd
      write(*,*) 
      write(*,*) " total_time_matrix_vector_products_mpi (seconds) --> ", total_time_mv_mpi
      write(*,*) " total_time_pdsaupd_subroutine_mpi (seconds) --> ", total_time_pdsaupd_mpi
      write(*,*) " total_time_pdseupd_mpi (seconds) --> ", total_time_pdseupd_mpi
      write(*,*) 
      write(*,*) "==========================================================================="
      write(*,*)

      write(*,*) "========================================================================"
      write(*,*) "  Indide driver program --> 3. "
      write(*,*) 
      write(*,*) " size of the matrix is --> ", n
      write(*,*) " the number of mpi processors is --> ", p
      write(*,*) " the number of ritz values requested is --> ", nev
      write(*,*) " the number of arnoldi vectors generated (ncv) is --> ", ncv
      write(*,*) " the portion of the spectrum is --> ", which
      write(*,*) " the number of converged ritz values is --> ", nconv 
      write(*,*) " the number of implicit arnoldi update itearions taken is --> ", iparam(3)
      write(*,*) " the number of op*x is --> ", iparam(9)
      write(*,*) " the convergence criterion is --> ", tol
      write(*,*) " "

    end if

  end if ! end of the main-if for production of the eigenpairs.

9000 continue

!========================================================================================
! 11. mpi at its best.
!========================================================================================

  allocate( array_loc(0:p-1) )

  ! 11.a. writing the eigenvalues.

  if ( my_rank == 0 .and. ierr == 0 ) then  

    open( unit=11,                             &
          file=va_file,                        &
          status="replace",                    &
          action="write",                      &
          form="formatted" )

    do i1 = 1, nev

      write(unit=11,fmt=*)  values_array(i1,1)

    end do

    close( unit=11, status="keep" )
 
  end if
    
  ! 11.b. writing the eigenvectors.

  ! setting the nloc.

  nloc = n/p

  if ( mod(n,p) /= 0 .and. my_rank < mod(n,p) ) then

    nloc = n/p + 1

  else if ( mod(n,p) /= 0 .and. my_rank >= mod(n,p) ) then

    nloc = n/p

  end if

  call MPI_Barrier( comm, ierr )

  ! writting the header of the files.

  write( unit=a_para+my_rank, fmt=* ) nloc, my_rank, n, p
  
  call MPI_Barrier( comm, ierr )

  ! writting the eigenvectors of the files.

  do i1 = 1, nev
    do i2 = 1, n  

      if ( abs(vectors_array(i2,i1)) > 0 ) then
      write( unit=a_para+my_rank,fmt=* )  vectors_array(i2,i1)
      end if

    end do
  end do

  ! writting the final file with the eigenvectors.

  rewind( unit=a_para+my_rank )

  if ( my_rank == 0 ) then

    ! reading the head of each file. 
    do i1 = 0, p-1        

      read(unit=a_para+i1,fmt=*) nloc, my_rank, n_new, p
      array_loc(i1) = nloc

    end do

    ! producing the final eigenvectors file.
    do i3 = 1, nev
      do i1 = 0, p-1
 
        nloc = array_loc(i1)       
        do i2 = 1, nloc

          read(unit=a_para+i1, fmt=*) help_var
          write(unit=5000, fmt=*) help_var

        end do
      end do
    end do

    ! cleaning 
    do i1 = 0, p-1 

      write( tmp_file_a, '(i4)' ) a_para+i1      
      tmp_file_b = "fort."//tmp_file_a     
      call system( "rm "//tmp_file_b )

    end do
    
    call system( "mv fort.5000 ___ve_mpi" )

  end if

  ! 11.c mpi finalization.

  call MPI_Finalize( ierr )

  end program driver_arpack_coo_mpi

!========================================================================================
! FINI.
!========================================================================================
