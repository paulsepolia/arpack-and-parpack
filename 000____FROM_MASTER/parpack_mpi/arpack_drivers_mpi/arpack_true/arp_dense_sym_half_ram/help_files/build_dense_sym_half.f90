
  program build_dense_sym_half
  
  implicit none

  ! 1. variables.

  real*8, allocatable, dimension(:)   :: matrix_dense_half
  integer*4                           :: matrix_dimen
  integer*4                           :: i1, i2
  character(len=200)                  :: matrix_file_name
  integer*8                           :: indx

  ! 2. giving input to the program.

  matrix_dimen = 1000
  matrix_file_name = "matrix_dense_sym_half"
  indx = 0

  ! 3. allocating ram space.

  allocate( matrix_dense_half( 1: (matrix_dimen**2 + matrix_dimen)/2 ) )

  matrix_dense_half = 0.0d0

  ! 4. building matrix in to ram.
    
  do i1 = 1, matrix_dimen
    do i2 = i1, matrix_dimen
 
      indx = indx + 1

      if ( i1 == i2 ) then
        matrix_dense_half(indx) = 2.5d0 * (i1+1.0d0) * (i2+1.0d0)
      else if ( i1 /= i2 ) then  
        matrix_dense_half(indx) = (i1+1.0d0) * (i2+1.0d0) / 3.0d0
      end if

    end do
  end do
   
  ! 5. open unit to write to the disk.

  open( unit=11,                 &
        file=matrix_file_name,   &
        form="unformatted",      &
        action="write",          &
        status="new" )      

  ! 6. writing to hard disk.
  
  ! a.
  write(unit=11) matrix_dimen 

  ! b.

  indx = 0 
  
  do i1 = 1, matrix_dimen
    do i2 = i1, matrix_dimen
   
      indx = indx + 1  
 
      write(unit=11)  matrix_dense_half(indx)
 
    end do
  end do
 
  ! 7. closing the unit.

  close(unit=11, status="keep")

  end program build_dense_sym_half

! FINI.
