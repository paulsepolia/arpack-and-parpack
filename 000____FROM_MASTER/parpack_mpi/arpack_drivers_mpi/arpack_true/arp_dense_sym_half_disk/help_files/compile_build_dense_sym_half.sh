#!/bin/bash

# 1. compiling the code

  ifort -O3 -static-intel                     \
  -parallel -par-threshold0                   \
  build_dense_sym_half.f90                    \
  -o x_build_dense_sym_half

# 2. compiling code without any optimization.

#  ifort -O0 i                                \
#  -traceback -check all                      \
#  -fltconsistency                            \
#  build_dense_sym_half.f90                   \
#  -o x_build_dense_sym_half                     

# 3. exiting.
