!====================================================================!
!                                                                    !
! Author: Pavlos G. Galiatsatos.                                     !
!                                                                    ! 
! Date: 2011/07/11.                                                  !
!                                                                   !
!====================================================================!

module m_2_matrix_coo_ram

  use m_1_type_definitions

  implicit none

  contains

  subroutine matrix_coo_ram( matrix_dimen,             &
                             non_zero_ham_elem,        &
                             row_index_array,          &            
                             column_index_array,       &
                             ham_elem_array )    
                                    

  implicit none

  ! 1.
  integer(kind=si), dimension(:), allocatable, intent(out) :: row_index_array
  integer(kind=si), dimension(:), allocatable, intent(out) :: column_index_array
  real(kind=dr)   , dimension(:), allocatable, intent(out) :: ham_elem_array
  integer(kind=si), intent(in) :: matrix_dimen
  integer(kind=di), intent(in) :: non_zero_ham_elem

  ! 2. local variables
  integer(kind=di) :: i1
  integer(kind=di) :: i2
  integer(kind=di) :: i_help 

  ! 3. allocating ram space.
  allocate( row_index_array     (1:non_zero_ham_elem) )
  allocate( column_index_array  (1:non_zero_ham_elem) )
  allocate( ham_elem_array      (1:non_zero_ham_elem) )
  
  ! 4. building the matrix. 
  !    it is symmetric here but can be ANY real matrix
  do i1 = 1, matrix_dimen
    do i2 = 1, matrix_dimen
    
      i_help = i2 + (i1-1) * matrix_dimen 
      ham_elem_array( i_help ) = dcos( real(i1, kind=dr) * real(i2, kind=dr) )

    end do
  end do

  ! 5. building the row_index_array & column_index_array
  do i1 = 1, matrix_dimen
    do i2 = 1, matrix_dimen

      i_help = i2 + (i1-1) * matrix_dimen
      row_index_array( i_help ) = int( i1, kind=si )
      column_index_array( i_help ) = int( i2, kind=si )     

    end do 
  end do

  end subroutine matrix_coo_ram

end module m_2_matrix_coo_ram
