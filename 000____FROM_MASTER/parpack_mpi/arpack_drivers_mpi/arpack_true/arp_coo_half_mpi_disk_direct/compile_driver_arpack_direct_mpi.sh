#!/bin/bash

# 1. compiling

#  mpiifort -O2  -static-intel -static_mpi    \
#  -L/opt/acml5.0.0/ifort64_mp/lib -lacml_mp  \
#  m_1_type_definitions.f90                   \
#  m_2_disk_direct_to_ram.f90                 \
#  m_3_smv_coo_half_mpi.f90                   \
#  driver_arpack_direct_mpi.f90               \
#  -L. -lparpack_intel_acml                   \
#  -L. -larpack_intel_acml                    \
#  -o x_mpi

  mpiifort -O2  -static-intel -static_mpi    \
  -mkl=cluster                               \
  m_1_type_definitions.f90                   \
  m_2_disk_direct_to_ram.f90                 \
  m_3_smv_coo_half_mpi.f90                   \
  driver_arpack_direct_mpi.f90               \
  -L. -lparpack_intel_mkl                    \
  -L. -larpack_intel_mkl                     \
  -o x_mpi

# 2. cleaning 

  rm *.mod
