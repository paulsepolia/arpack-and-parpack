!====================================================================!
!                                                                    !
! Author: Pavlos G. Galiatsatos.                                     !
!                                                                    !
! Date: 2011/07/16.                                                  !
!                                                                    !
!====================================================================!

  program driver_arpack_direct_mpi

  use mpi
  use m_1_type_definitions
  use m_2_disk_direct_to_ram
  use m_3_smv_coo_half_mpi

  implicit none
   
!========================================================================================
! 1. interface variables.
!========================================================================================

  integer(kind=si), parameter  :: nev         = 20              !  1. number of eigenvalues
  integer(kind=si), parameter  :: ncv_coeff   = 2               !  2. the size of arnoldi space
  real(kind=dr)                :: tol         = 0.0_dr          !  3. tolerance
  character(len=1)             :: bmat        = 'I'             !  4. normal problem
  character(len=2)             :: which       = 'SA'            !  5. the part of the spectrum
  character(len=200),parameter :: va_file     = '___va_mpi'     !  6. file name for eigenvalues
  character(len=200),parameter :: ve_file     = '___ve_mpi'     !  7. file name for eigenvectors
  character(len=200),parameter :: matrix_name = "matrix_direct" ! 8. matrix file name on disk 

!========================================================================================
! DO NOT MODIFY AYTHING BELOW THIS LINE. 
! 2. declaration of any other parameter and variable.
!========================================================================================
  
  integer(kind=si), parameter                   :: maxitr = 3000000
  integer(kind=si), parameter                   :: ncv    = ncv_coeff*nev+1
  integer(kind=si)                              :: maxnloc
  integer(kind=si)                              :: n
  integer(kind=si), parameter                   :: maxnev = nev+10
  integer(kind=si), parameter                   :: maxncv = ncv+10
  integer(kind=si)                              :: ldv 
  integer(kind=si)                              :: comm
  integer(kind=si)                              :: nloc
  real(kind=dr),    allocatable, dimension(:)   :: workl
  real(kind=dr),    allocatable, dimension(:)   :: workd
  real(kind=dr),    allocatable, dimension(:)   :: resid
  logical,          allocatable, dimension(:)   :: select_my
  integer(kind=si), allocatable, dimension(:)   :: iparam
  integer(kind=si), allocatable, dimension(:)   :: ipntr
  integer(kind=si)                              :: ido
  integer(kind=si)                              :: lworkl
  integer(kind=si)                              :: info
  integer(kind=si)                              :: ierr
  integer(kind=si)                              :: j
  integer(kind=si)                              :: ndigit
  integer(kind=si)                              :: logfil
  integer(kind=si)                              :: nconv
  integer(kind=si)                              :: mode
  integer(kind=si)                              :: ishfts
  integer(kind=si)                              :: msaupd
  logical                                       :: rvec
  real(kind=dr)                                 :: sigma
  real(kind=dr)                                 :: pdnorm2 
  real(kind=dr),    allocatable, dimension(:,:) :: vectors_array
  real(kind=dr),    allocatable, dimension(:,:) :: values_array
  integer(kind=si)                              :: matrix_dimen  
  integer(kind=si)                              :: my_rank
  integer(kind=si)                              :: p
  integer(kind=si), allocatable, dimension(:)   :: row_index_array
  integer(kind=si), allocatable, dimension(:)   :: column_index_array
  real(kind=dr),    allocatable, dimension(:)   :: ham_elem_array
  integer(kind=di)                              :: i1
  integer(kind=di)                              :: i2
  real(kind=dr), allocatable, dimension(:)      :: x
  real(kind=dr), allocatable, dimension(:)      :: y
  real(kind=dr), allocatable, dimension(:)      :: y_final
  real(kind=dr)                                 :: t1_mpi
  real(kind=dr)                                 :: t2_mpi
  real(kind=dr)                                 :: t1
  real(kind=dr)                                 :: t2
  integer(kind=di)                              :: non_zero_ham_elem_half
  integer(kind=di)                              :: non_zero_a
  real(kind=dr)                                 :: total_time_pdsaupd
  real(kind=dr)                                 :: total_time_pdseupd
  real(kind=dr)                                 :: total_time_mv
  real(kind=dr)                                 :: total_time_pdsaupd_mpi
  real(kind=dr)                                 :: total_time_pdseupd_mpi
  real(kind=dr)                                 :: total_time_mv_mpi
  real(kind=dr), dimension(:), allocatable      :: x_help 
  integer(kind=si)                              :: lobo
  integer(kind=si)                              :: upbo
  integer(kind=di)                              :: rec_len
  real(kind=di), dimension(1)                   :: dimen_line

!========================================================================================
! 3. starting mpi.
!========================================================================================
  
  call MPI_Init( ierr )

  comm = MPI_COMM_WORLD

  call MPI_Comm_rank( comm, my_rank, ierr )

  call MPI_Comm_size( comm, p, ierr )

!========================================================================================
! 4. calling the subroutine to read the matrix.
!========================================================================================

  call disk_direct_to_ram( comm,                         &
                           my_rank,                      &
                           p,                            &
                           matrix_dimen,                 &
                           non_zero_ham_elem_half,       &
                           row_index_array,              &
                           column_index_array,           &
                           ham_elem_array,               &
                           non_zero_a,                   &
                           matrix_name )

!========================================================================================
! 5. parpack allocations and definitions.
!========================================================================================

  n = matrix_dimen 
  maxnloc =  n + 100
  ldv = maxnloc

  allocate ( workl(1:maxncv*(maxncv+8)) )
  allocate ( workd(1:3*maxnloc) )
  allocate ( resid(1:maxnloc) )
  allocate ( select_my(1:maxncv) )
  allocate ( iparam(1:11) )
  allocate ( ipntr(1:11) )  
  allocate ( vectors_array(1:ldv,maxncv) )
  allocate ( values_array(maxncv,2) )
  
  ndigit = -3
  logfil =  6
  msaupd =  1

! mpi the krylov space
  
  nloc = n/p

  if ( mod(n,p) /= 0 .and. my_rank < mod(n,p) ) then

    nloc = n/p + 1

  else if ( mod(n,p) /= 0 .and. my_rank >= mod(n,p) ) then

    nloc = n/p

  end if


!========================================================================================
! 6. checking for errors.
!========================================================================================

  if ( nloc > maxnloc ) then

    write(*,*), " ERROR with _SDRV1: NLOC is greater than MAXNLOC "
    go to 9000

  else if ( nev > maxnev ) then

    write(*,*), " ERROR with _SDRV1: NEV is greater than MAXNEV "
    go to 9000

  else if ( ncv > maxncv ) then

    write(*,*), " ERROR with _SDRV1: NCV is greater than MAXNCV "
    go to 9000

  end if

!========================================================================================
! 7. further executable statements.
!========================================================================================

  ! 7.a. zeroed timing variables.

  total_time_mv          = 0.0_dr 
  total_time_mv_mpi      = 0.0_dr
  total_time_pdsaupd     = 0.0_dr
  total_time_pdsaupd_mpi = 0.0_dr
  total_time_pdseupd     = 0.0_dr
  total_time_pdseupd_mpi = 0.0_dr

  ! 7.b. parpack definitions.

  lworkl    = ncv*(ncv+8)
  info      = 0
  ido       = 0
  ishfts    = 1
  mode      = 1
  iparam(1) = ishfts 
  iparam(3) = maxitr 
  iparam(7) = mode 

!========================================================================================
! 8. main lanczos loop.
!========================================================================================

  ! 8.a. allocations of the vectors. 

  allocate( x_help(1:matrix_dimen) )
  allocate( x(1:matrix_dimen) )
  allocate( y(1:matrix_dimen) )
  allocate( y_final(1:matrix_dimen) )

  ! 8.b. main loop. the "biggest goto loop" --> starts.

  10 continue

  if ( my_rank == 0 ) then
    call cpu_time(t1)
  end if
  t1_mpi = MPI_Wtime()

  call pdsaupd ( comm,            &   !  0.
                 ido,             &   !  1.
                 bmat,            &   !  2.
                 nloc,            &   !  3. -->  local matrix dimen.
                 which,           &   !  4.
                 nev,             &   !  5.
                 tol,             &   !  6.
                 resid,           &   !  7.
                 ncv,             &   !  8.
                 vectors_array,   &   !  9.
                 ldv,             &   ! 10.
                 iparam,          &   ! 11.
                 ipntr,           &   ! 12.
                 workd,           &   ! 13.
                 workl,           &   ! 14.
                 lworkl,          &   ! 15.
                 info )               ! 16.

  if ( my_rank == 0 ) then
    call cpu_time(t2)
  end if
  t2_mpi = MPI_Wtime()

  total_time_pdsaupd     = total_time_pdsaupd + (t2-t1)             ! timing pdsaupd.
  total_time_pdsaupd_mpi = total_time_pdsaupd_mpi + (t2_mpi-t1_mpi) ! timing pdsaupd_mpi.

  if (ido == -1 .or. ido == 1) then  ! the "biggest if" --> starts.

    ! 8.c. setting to zero the x, x_help     

    x_help = 0.0_dr
    x = 0.0_dr    
  
    ! 8.d. building the x_help

    if ( mod(n,p) == 0 ) then

      lobo = my_rank*nloc+1
      upbo = (my_rank+1)*nloc

      x_help(lobo:upbo) = workd(ipntr(1):ipntr(1)+nloc)

    end if


    if ( mod(n,p) /= 0 ) then

      if ( my_rank < mod(n,p) ) then
 
        lobo = my_rank*nloc+1
        upbo = (my_rank+1)*nloc

        x_help(lobo:upbo) = workd(ipntr(1):ipntr(1)+nloc)

      else if ( my_rank == mod(n,p) ) then

        lobo = my_rank*(nloc+1)+1
        upbo = my_rank*(nloc+1)+1+nloc

        x_help(lobo:upbo) = workd(ipntr(1):ipntr(1)+nloc)

      else if ( my_rank > mod(n,p) ) then

        lobo = mod(n,p)*(nloc+1)+1+nloc+1*((my_rank-1-mod(n,p))*nloc)
        upbo = mod(n,p)*(nloc+1)+1+nloc+1*((my_rank-0+mod(n,p))*nloc)

        x_help(lobo:upbo ) = workd(ipntr(1):ipntr(1)+nloc)

      end if

    end if

    call MPI_Barrier( comm, ierr )

    ! 8.e. reducing the x_help vector to the master thread

    if ( my_rank == 0 ) then
      call cpu_time(t1)
    end if
    t1_mpi = MPI_Wtime()

    call MPI_Reduce( x_help,                  &
                     x,                       &
                     matrix_dimen,            &
                     MPI_DOUBLE_PRECISION,    &               
                     MPI_SUM,                 &
                     0,                       &
                     MPI_COMM_WORLD,          &
                     ierr )

    call MPI_Barrier( comm, ierr )

!========================================================================================
! 9. the mpi sparse matrix - vector product 
!=======================================================================================

    !  9.a. reseting the y help local vector.

    y = 0.0_dr

    !  9.b. the smv mpi routine.

    call smv_coo_half_mpi( comm,                     &    !  1.
                           my_rank,                  &    !  2.
                           p,                        &    !  3.
                           matrix_dimen,             &    !  4.
                           non_zero_ham_elem_half,   &    !  5.
                           row_index_array,          &    !  6.
                           column_index_array,       &    !  7.
                           ham_elem_array,           &    !  8.
                           non_zero_a,               &    !  9.
                           x,                        &    ! 10.
                           y,                        &    ! 11.
                           y_final )                      ! 12.

    call MPI_Bcast( y_final, matrix_dimen, MPI_DOUBLE_PRECISION, 0, comm, ierr )

    call MPI_Barrier( comm, ierr )

    !  9.c. the resultant y_final vector.

    if ( mod(n,p) == 0 ) then

      workd(ipntr(2):ipntr(2)+nloc) = y_final(my_rank*nloc+1:(my_rank+1)*nloc)            
  
    end if

    if ( mod(n,p) /= 0 ) then 

      if ( my_rank < mod(n,p) ) then

        lobo = my_rank*nloc+1
        upbo = (my_rank+1)*nloc 

        workd(ipntr(2):ipntr(2)+nloc) = y_final(lobo:upbo)            

      else if ( my_rank == mod(n,p) ) then

        lobo = my_rank*(nloc+1)+1
        upbo = my_rank*(nloc+1)+1+nloc      

        workd(ipntr(2):ipntr(2)+nloc) = y_final(lobo:upbo )

      else if ( my_rank > mod(n,p) )  then

        lobo = mod(n,p)*(nloc+1)+1+nloc+1*((my_rank-1-mod(n,p))*nloc)      
        upbo = mod(n,p)*(nloc+1)+1+nloc+1*((my_rank-0-mod(n,p))*nloc)      
       
        workd(ipntr(2):ipntr(2)+nloc ) = y_final(lobo:upbo)   

      end if

    end if

    if ( my_rank == 0 ) then
      call cpu_time(t2)
    end if
    t2_mpi = MPI_Wtime()

    total_time_mv     = total_time_mv + (t2-t1)             ! timing mv.
    total_time_mv_mpi = total_time_mv_mpi + (t2_mpi-t1_mpi) ! timing mv_mpi.

    ! 9.d. "goto loop" again if necessary.
    go to 10
  
  end if ! the "biggest if" --> ends.

!========================================================================================
! 10. some checks and building the vectors.
!========================================================================================

  ! 10.a. error checking.

  if ( info < 0 ) then

    if ( my_rank == 0 ) then
            
      write(*,*), " "
      write(*,*), " Error with _saupd, info = ", info
      write(*,*), " Check documentation in _saupd "
      write(*,*), " "
    
    endif

  else 
  
    ! 10.b. production of the eigenpairs. 

    rvec = .true.

    if ( my_rank == 0 ) then
      call cpu_time(t1)
    end if
    t1_mpi = MPI_Wtime()

    call pdseupd ( comm,           &     !  0.
                   rvec,           &     !  1.
                   'All',          &     !  2.
                   select_my,      &     !  3.
                   values_array,   &     !  4.
                   vectors_array,  &     !  5.
                   ldv,            &     !  6.
                   sigma,          &     !  7.
                   bmat,           &     !  8.
                   nloc,           &     !  9.
                   which,          &     ! 10.
                   nev,            &     ! 11.
                   tol,            &     ! 12.
                   resid,          &     ! 13.
                   ncv,            &     ! 14.
                   vectors_array,  &     ! 15.
                   ldv,            &     ! 16.
                   iparam,         &     ! 17.
                   ipntr,          &     ! 18.
                   workd,          &     ! 19.
                   workl,          &     ! 20.
                   lworkl,         &     ! 21.
                   ierr )                ! 22.

    if ( my_rank == 0 ) then
      call cpu_time(t2)
    end if
    t2_mpi = MPI_Wtime()

    total_time_pdseupd     = total_time_pdseupd + (t2-t1) ! timing pdseupd.
    total_time_pdseupd_mpi = total_time_pdseupd_mpi + (t2_mpi-t1_mpi) ! timing pdseupd_mpi.

    ! 10.c. further error checks.
                   
    if ( ierr /= 0) then
      if ( my_rank == 0 ) then

        write(*,*), " "
        write(*,*), " Error with _seupd, info = ", ierr
        write(*,*), " Check the documentation of _seupd. "
        write(*,*), " "

      end if
    end if

    ! 10.d. outputs. 

    if ( ierr == 0 .and. my_rank == 0 ) then
             
      write(*,*) "==========================================================================="
      write(*,*) " Inside driver program --> 1."
      write(*,*) 
      write(*,*) " Some characteristics values of the matrix under diagonalization."
      write(*,*) 
      write(*,*) " matrix_dimen --> ", matrix_dimen
      write(*,*) " non_zero_ham_elem_half --> ", non_zero_ham_elem_half
      write(*,*) " row_index_array(100) --> ", row_index_array(100)
      write(*,*) " column_index_array(100) --> ", column_index_array(100)
      write(*,*) " ham_elem_array(100) --> ", ham_elem_array(100)
      write(*,*) 
      write(*,*) "==========================================================================="
      write(*,*)

      write(*,*) "==========================================================================="
      write(*,*) " Inside driver program --> 2. "
      write(*,*) 
      write(*,*) " total_time_matrix_vector_products (seconds) --> ", total_time_mv
      write(*,*) " total_time_pdsaupd_subroutine (seconds) --> ", total_time_pdsaupd
      write(*,*) " total_time_pdseupd (seconds) --> ", total_time_pdseupd
      write(*,*) 
      write(*,*) " total_time_matrix_vector_products_mpi (seconds) --> ", total_time_mv_mpi
      write(*,*) " total_time_pdsaupd_subroutine_mpi (seconds) --> ", total_time_pdsaupd_mpi
      write(*,*) " total_time_pdseupd_mpi (seconds) --> ", total_time_pdseupd_mpi
      write(*,*) 
      write(*,*) "==========================================================================="
      write(*,*)

      write(*,*) "========================================================================"
      write(*,*) "  Indide driver program --> 3. "
      write(*,*) 
      write(*,*) " size of the matrix is --> ", n
      write(*,*) " the number of mpi processors is --> ", p
      write(*,*) " the number of ritz values requested is --> ", nev
      write(*,*) " the number of arnoldi vectors generated (ncv) is --> ", ncv
      write(*,*) " the portion of the spectrum is --> ", which
      write(*,*) " the number of converged ritz values is --> ", nconv 
      write(*,*) " the number of implicit arnoldi update itearions taken is --> ", iparam(3)
      write(*,*) " the number of op*x is --> ", iparam(9)
      write(*,*) " the convergence criterion is --> ", tol
      write(*,*) 

    end if

  end if ! end of the main-if for production of the eigenpairs.

9000 continue

!========================================================================================
! 11. mpi at its best.
!========================================================================================

  ! 11.a. writing the eigenvalues.

  if ( my_rank == 0 .and. ierr == 0 ) then  

    open( unit=11,                             &
          file=va_file,                        &
          status="replace",                    &
          action="write",                      &
          form="formatted" )

    do i1 = 1, nev

      write(unit=11,fmt=*)  values_array(i1,1)

    end do

    close( unit=11, status="keep" )
 
  end if
    
  ! 11.b. writing the eigenvectors.

  ! setting the nloc.

  nloc = n/p

  if ( mod(n,p) /= 0 .and. my_rank < mod(n,p) ) then

    nloc = n/p + 1

  else if ( mod(n,p) /= 0 .and. my_rank >= mod(n,p) ) then

    nloc = n/p

  end if

  call MPI_Barrier( comm, ierr )

  ! setting the common record length

  inquire( iolength = rec_len ) dimen_line

  open( unit = 18,              & 
        form = "unformatted",   &
        file = ve_file,         &
        status = "replace",     &
        action = "write",       &
        access = "direct",      &
        recl = rec_len )    

  call MPI_Barrier( comm, ierr )

  ! writting to the hard disk the eigenvectors 
  ! case: mod(n,p) == 0

  if ( mod(n,p) == 0 ) then

    do i1 = 1, nev
      do i2 =1, nloc     

        write( unit=18, rec=i2+nloc*my_rank+(i1-1)*n )   &
        
        vectors_array(i2,i1)

      end do
    end do

  end if

  call MPI_Barrier( comm, ierr )

  ! writting to the hard disk the eigenvectors: 
  ! case: mod(n,p) /= 0 && my_rank < mod(n,p)

  if ( mod(n,p) /= 0 .and. my_rank < mod(n,p) ) then

    do i1 = 1, nev
      do i2 =1, nloc     

        write( unit=18, rec=i2+nloc*my_rank+(i1-1)*n )   &
        
        vectors_array(i2,i1)

      end do
    end do
  
  end if

  call MPI_Barrier( comm, ierr )

  ! writting to the hard disk the eigenvectors: 
  ! case: mod(n,p) /= 0 && my_rank >= mod(n,p)

  if ( mod(n,p) /= 0 .and. my_rank >= mod(n,p) ) then

    do i1 = 1, nev
      do i2 =1, nloc     

        write( unit=18, rec=i2+(nloc+1)*mod(n,p) +              & 
                            nloc*(my_rank-mod(n,p))+(i1-1)*n )  &
        
        vectors_array(i2,i1)

      end do
    end do
  
  end if

  ! 11.c closing the unit for the eigenvectors

  close( unit=18, status="keep" )

  ! 11.d mpi finalization.

  call MPI_Finalize( ierr )

  end program driver_arpack_direct_mpi

!========================================================================================
! FINI.
!========================================================================================
