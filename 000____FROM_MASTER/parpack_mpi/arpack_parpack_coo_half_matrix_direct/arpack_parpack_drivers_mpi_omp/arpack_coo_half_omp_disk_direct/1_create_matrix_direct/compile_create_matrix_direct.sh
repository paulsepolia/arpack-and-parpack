#!/bin/bash

# 1. compiling

  ifort -O2 -static-intel    \
  m_1_type_definitions.f90   \
  create_matrix_direct.f90   \
  -o x_create_matrix_direct

# 2. cleaning 

  rm *.mod
