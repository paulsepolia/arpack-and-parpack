#!/bin/bash

# 1. compiling the code

  ifort -O3 -static-intel                                 \
  -parallel -par-threshold0                               \
  -par-report1                                            \
  -mkl=parallel                                           \
  -openmp                                                 \
  -openmp-report2                                         \
  m_1_type_definitions.f90                                \
  m_2_coo_half_disk_direct.f90                            \
  m_3_arpack_omp.f90                                      \
  driver_arpack_omp.f90                                   \
  -L. -larpack_intel_mkl                                  \
  -o x_arpack_omp

# 2. cleaning the *.mod produced files.

  rm *.mod

# 3. exiting.
