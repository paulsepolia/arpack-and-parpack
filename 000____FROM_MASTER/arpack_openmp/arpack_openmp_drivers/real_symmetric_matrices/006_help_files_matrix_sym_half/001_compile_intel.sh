#!/bin/bash

# 1. compiling the code

  ifort -O3                        \
	    -e08                       \
        -static                    \
        -parallel                  \
        -par-threshold0            \
        -assume buffered_io        \
        build_dense_sym_half.f90   \
        -o x_intel

# 2. exiting
