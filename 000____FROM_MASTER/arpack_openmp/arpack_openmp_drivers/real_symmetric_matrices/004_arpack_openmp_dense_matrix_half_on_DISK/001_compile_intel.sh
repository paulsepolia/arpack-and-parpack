#!/bin/bash

# 1. compiling the code

  ifort  -O3                                  \
         -e03                                 \
         -static                              \
         -openmp                              \
	     -cpp                                 \
         m_1_type_definitions.f90             \
         m_2_matrix_dense_sym_half_to_ram.f90 \
         m_3_mat_dense_half_vec_product.f90   \
         m_4_arp_dense_sym_half.f90           \
	     driver_program.f90                   \
         /opt/arpack/lib_2015/libarpack_intel.a \
         -o x_intel

# 2. cleaning the *.mod produced files

  rm *.mod

# 3. exiting
