!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2014/12/10              !
!===============================!

module m_1_type_definitions

  implicit none
 
!==================================================!
! 1. common type definitions                       !         
!                                                  !
! a. here i define all the common type definitions !
! b. these kind types are used in any module       ! 
!==================================================!  

  integer, parameter :: dr = selected_real_kind(p=15,r=300) 
  integer, parameter :: sr = selected_real_kind(p=6,r=30) 
  integer, parameter :: di = selected_int_kind(18) 
  integer, parameter :: si = selected_int_kind(9) 
  
end module m_1_type_definitions

! FINI
