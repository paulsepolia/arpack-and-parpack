
!===================================!
! Driver program to ARPACK-OpenMP   !
! for real dense symmetric matrices !
!===========================================!
! Author: Pavlos G. Galiatsatos, 2014/12/11 !
!===========================================!

program driver_arpack_dense_sym_half_ram

  use m_1_type_definitions
  use m_4_arpack_dense_sym_half

  implicit none

! 1. driver/input variables

  real(kind=dr), allocatable, dimension(:)   :: matrix
  integer(kind=si)                           :: matrix_dimen
  character(len=200)                         :: matrix_file_name
  character(len=200)                         :: type_of_format_for_matrix
  real(kind=dr), allocatable, dimension(:,:) :: vectors_array
  real(kind=dr), allocatable, dimension(:,:) :: values_array
  integer(kind=si)                           :: nev
  integer(kind=si)                           :: maxitr
  real(kind=dr)                              :: tol
  character(len=2)                           :: which
  integer(kind=si)                           :: ncv_coeff

! 2. local variables for outputting the produced eigensystem

  character(len=200) :: type_of_format_for_output
  character(len=200) :: file_for_eigenvalues
  character(len=200) :: file_for_eigenvectors
 
! 3. local variables and parameters
  
  real(kind=dr), parameter :: zero = 0.0_dr
  integer(kind=si) :: i1
  integer(kind=si) :: i2
  integer(kind=di) :: indx
  integer(kind=di) :: dim_total
  integer(kind=si), parameter :: URW = 11


!========================================================================================!
!========================================================================================!
! 4. all the necessary input for the driver


  ! 4-1. building the matrix - just an example or real symmetric matrix

  ! a.

  matrix_dimen = 500
  dim_total = (int(matrix_dimen,kind=di)**2 + matrix_dimen)/2 

  ! b.

  allocate(matrix(1:dim_total))
  indx = 0

  ! c.

  do i1 = 1, matrix_dimen
    do i2 = i1, matrix_dimen
    
      indx = indx + 1

      if (i1 == i2) then
        matrix(indx) = 2.5d0 * (i1+1.0d0) * (i2+1.0d0)
      else if (i1 /= i2) then
        matrix(indx) = (i1+1.0d0) * (i2+1.0d0) / 3.0d0
      end if

    end do
  end do

  ! 4-2. type of format of the output eigensystem

  type_of_format_for_output = "formatted"

  ! 4-3. name of the file for eigenvalues

  file_for_eigenvalues = "___va"

  ! 4-4. name of the file for eigenvectors

  file_for_eigenvectors = "___ve"

  ! 4-5. number of eigenvalues/eigenvectors to get back

  nev = 50 

  ! 4-6. maximum arnoldi restarts

  maxitr = 3000000

  ! 4-7. the convergence criterion

  tol = zero
 
  ! 4-8. the part of the spectrum

  which = 'SA'

  ! 4-9. the part 'ncv_coeff'

  ncv_coeff = 2

!========================================================================================!
!========================================================================================!

! 5. testing. outputting some characteristics of the matrix: "matrix_file_name"

  write(*,*) "==========================================================================="
  write(*,*) "==========================================================================="
  write(*,*) " Inside driver program"
  write(*,*) 
  write(*,*) " Some characteristics values of the matrix under diagonalization"
  write(*,*) 
  write(*,*) " matrix_dimen --> ", matrix_dimen
  write(*,*) " matrix(1)    --> ", matrix(1)
  write(*,*) " matrix(2)    --> ", matrix(2)
  write(*,*) " matrix(3)    --> ", matrix(3)
  write(*,*) " matrix(4)    --> ", matrix(4)
  write(*,*) "==========================================================================="

! 6. calling the subroutine which diagonalizes the matrix: "matrix_file_name"

  call arpack_dense_sym_half(matrix,        &  !  1. input
                             matrix_dimen,  &  !  2. input
                             which,         &  !  3. input
                             ncv_coeff,     &  !  4. input
                             nev,           &  !  5. input
                             maxitr,        &  !  6. input
                             tol,           &  !  7. input
                             vectors_array, &  !  8. output
                             values_array)     !  9. output

  
! 7. writing to the hard disk the eigensystem

  if (type_of_format_for_output == "unformatted") then

    ! a. eigenvalues area. unformatted output

    open(unit=URW,                       &
         file=file_for_eigenvalues,      &
         status="new",                   &
         action="write",                 &
         form=type_of_format_for_output)

    do i1 = 1, nev

      write(unit=URW) values_array(i1,1)

    end do

    flush(unit=URW)
  
    close(unit=URW, status="keep")

    ! b. eigenvectors area. unformatted output

    open(unit=URW,                       &
         file=file_for_eigenvectors,     &
         status="new",                   &
         action="write",                 &
         form=type_of_format_for_output)


    do i1 = 1, nev
      do i2 = 1, matrix_dimen

        write(unit=URW) vectors_array(i2,i1)

      end do
    end do

    flush(unit=URW)

    close(unit=URW, status="keep")

  else if (type_of_format_for_output == "formatted") then

    ! c. eigenvalues area. formatmatted output

    open(unit=URW,                       &
         file=file_for_eigenvalues,      &
         status="new",                   &
         action="write",                 &
         form=type_of_format_for_output)

    do i1 = 1, nev

      write(unit=URW,fmt=*) values_array(i1,1)

    end do

    flush(unit=URW)

    close(unit=URW, status="keep")

    ! d. eigenvectors area. formatted output

    open(unit=URW,                       &
         file=file_for_eigenvectors,     &
         status="new",                   &
         action="write",                 &
         form=type_of_format_for_output)


    do i1 = 1, nev
      do i2 = 1, matrix_dimen

        write(unit=URW,fmt=*) vectors_array(i2,i1)

      end do
    end do

    flush(unit=URW)

    close(unit=URW, status="keep")

  else 

    write(*,*) " error: type of format must be 'formatted' of 'unformatted'"
    stop   

  end if

! 8. deallocations

  deallocate(matrix)
  deallocate(values_array)
  deallocate(vectors_array)

end program driver_arpack_dense_sym_half_ram

! FINI
