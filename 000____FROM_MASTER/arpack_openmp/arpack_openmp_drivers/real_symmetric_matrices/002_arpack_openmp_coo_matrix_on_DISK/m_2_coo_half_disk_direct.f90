!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2014/12/10              !
!===============================!

module m_2_coo_half_disk_direct

  use m_1_type_definitions

  implicit none

  contains

  subroutine coo_half_direct_to_ram(matrix_file,            &
                                    type_of_format,         & 
                                    matrix_dimen,           &
                                    non_zero_ham_elem_half, &
                                    row_index_array,        &            
                                    column_index_array,     &
                                    ham_elem_array)    
                                    

  implicit none

  ! 1.

  character(len=200) :: matrix_file
  character(len=200) :: type_of_format

  integer(kind=si), dimension(:), allocatable, intent(out) :: row_index_array
  real(kind=dr) :: row_tmp
  integer(kind=si), dimension(:), allocatable, intent(out) :: column_index_array
  real(kind=dr) :: col_tmp
  real(kind=dr)   , dimension(:), allocatable, intent(out) :: ham_elem_array
 
  integer(kind=si), intent(out) :: matrix_dimen
  real(kind=dr)                 :: matrix_dimen_tmp
  integer(kind=di), intent(out) :: non_zero_ham_elem_half
  real(kind=dr)                 :: non_tmp
  real(kind=dr), dimension(3) :: dimen_line
  integer(kind=si) :: rec_len

  ! 2. local variables

  integer(kind=di) :: i1
 
  ! 3.
  
  inquire(iolength = rec_len) dimen_line ! evaluating the record length

  open(unit = 11,             &
       form = type_of_format, &
       file = matrix_file,    &
       status = "old",        &
       action = "read",       &
       access = "direct",     &
       recl = rec_len)  

  ! 4.

  if (type_of_format == "unformatted") then 

    read(unit=11, rec=1) dimen_line(1), &
                         dimen_line(2), &
                         dimen_line(3) 

    matrix_dimen_tmp = dimen_line(1)
    matrix_dimen_tmp = dimen_line(2)
    non_tmp          = dimen_line(3)

  else 

    write(*,*)  " error in the chosen format: ", type_of_format
    write(*,*)  " the chosen format must be 'unformatted'" 
    stop
 
  end if

  matrix_dimen = int(matrix_dimen_tmp, kind=si)  ! 1st conversion
  non_zero_ham_elem_half = int(non_tmp, kind=di) ! 2nd conversion

  ! 5.

  allocate(row_index_array(1:non_zero_ham_elem_half))
  allocate(column_index_array(1:non_zero_ham_elem_half))
  allocate(ham_elem_array(1:non_zero_ham_elem_half))
  
  ! 6.
  
  do i1 = 1 , non_zero_ham_elem_half

    read(unit=11, rec = i1+1) dimen_line(1),  &
                              dimen_line(2),  &
                              dimen_line(3)

    row_tmp            = dimen_line(1)
    col_tmp            = dimen_line(2)
    ham_elem_array(i1) = dimen_line(3)
    
    row_index_array(i1)    = int(row_tmp, kind=si) ! 3nd conversion
    column_index_array(i1) = int(col_tmp, kind=si) ! 4th conversion

  end do 

  ! 7. 
 
  close(unit=11, status="keep")

  end subroutine coo_half_direct_to_ram

end module m_2_coo_half_disk_direct

! FINI
