#!/bin/bash

# 1. compiling the code

  ifort  -O3                          \
	     -e03                         \
         -static                      \
         -parallel                    \
         -par-threshold0              \
         -openmp                      \
	     -cpp                         \
         -assume buffered_io          \
         m_1_type_definitions.f90     \
         m_2_coo_half_disk_direct.f90 \
          m_3_arp_coo_half_omp.f90    \
         driver_program.f90           \
         /opt/arpack/lib_2015/libarpack_intel.a \
         -o x_intel

# 2. cleaning the *.mod produced files

  rm *.mod

# 3. exiting
