
!=================================!
! Driver program to ARPACK-OpenMP !
! for real symmetric matrices     !
!===========================================!
! Author: Pavlos G. Galiatsatos, 2014/12/10 !
!===========================================!

program driver_arp_coo_omp_ram

  use m_1_type_definitions
  use m_2_matrix_coo_ram
  use m_3_arp_coo_omp_ram

#ifdef _OPENMP
  use omp_lib
#endif

  implicit none

! 1. local parameters and variables

  integer(kind=si), parameter :: matrix_dimen = 1000
  integer(kind=di)            :: non_zero_ham_elem
  integer(kind=si), allocatable, dimension(:) :: row_index_array
  integer(kind=si), allocatable, dimension(:) :: column_index_array
  real(kind=dr)   , allocatable, dimension(:) :: ham_elem_array
  real(kind=dr), allocatable,  dimension(:,:) :: vectors_array
  real(kind=dr), allocatable,  dimension(:,:) :: values_array
  integer(kind=si) :: nev     
  integer(kind=si) :: maxitr
  real(kind=dr)    :: tol 
  character(len=2) :: which
  integer(kind=si) :: ncv_coeff
  integer(kind=si) :: UNIT_RW = 11

! 2. local variables for outputting the produced eigensystem

  character(len=200) :: type_of_format_for_output
  character(len=200) :: file_for_eigenvalues
  character(len=200) :: file_for_eigenvectors
 
! 3. local variables and parameters
  
  real(kind=dr), parameter :: zero = 0.0_dr
  integer(kind=si) :: i1, i2

  non_zero_ham_elem = matrix_dimen * real( matrix_dimen, kind=dr )

!========================================================================================!
!========================================================================================!
! 4. All the necessary input for the driver

  ! 4-1. type of format of the output eigensystem

  type_of_format_for_output = "formatted"

  ! 4-2. name of the file for eigenvalues

  file_for_eigenvalues = "___va_coo_ram"

  ! 4-3. name of the file for eigenvectors

  file_for_eigenvectors = "___ve_coo_ram"

  ! 4-4. number of eigenvalues/eigenvectors to get back

  nev = 200

  ! 4-5. maximum arnoldi restarts

  maxitr = 3000000

  ! 4-6. the convergence criterion

  tol = zero 

  ! 4-7. the part of the spectrum

  which = 'SA'

  ! 4-8. the part 'ncv_coeff'

  ncv_coeff = 2

!========================================================================================!
!========================================================================================!

! 5. calling the subroutine which builds the matrix in coordinate format 
!
!    a. dimension of the matrix                     : "matrix_dimen"
!    b. the number of non zero hamiltonian elements : "non_zero_ham_elem"
!    c. the indexes of rows                         : "row_index_array"
!    d. the indexes of columns                      : "column_index_array"
!    e. the corresponing hamiltonian elements       : "ham_elm_array"

  call  matrix_coo_ram(matrix_dimen,       &
                       non_zero_ham_elem,  &
                       row_index_array,    &
                       column_index_array, &
                       ham_elem_array)

! 6. testing. outputting some characteristics of the matrix: "matrix_file_name"

  write(*,*) "==========================================================================="
  write(*,*) "==========================================================================="
  write(*,*) " Inside driver program"
  write(*,*) 
  write(*,*) " Some characteristics values of the matrix under diagonalization"
  write(*,*) 
  write(*,*) " matrix_dimen            --> ", matrix_dimen
  write(*,*) " non_zero_ham_elem       --> ", non_zero_ham_elem
  write(*,*) " row_index_array(100)    --> ", row_index_array(100)
  write(*,*) " column_index_array(100) --> ", column_index_array(100)
  write(*,*) " ham_elem_array(100)     --> ", ham_elem_array(100)
  write(*,*) 
  write(*,*) "==========================================================================="

! 7. calling the subroutine which diagonalizes the matrix

  call arp_coo_omp_auto(row_index_array,    &  !   1. input & deallocated
                        column_index_array, &  !   2. input & deallocated
                        ham_elem_array,     &  !   3. input & deallocated
                        matrix_dimen,       &  !   4. input & output
                        non_zero_ham_elem,  &  !   5. input & output
                        which,              &  !   6. input
                        ncv_coeff,          &  !   7. input
                        nev,                &  !   8. input & output
                        maxitr,             &  !   9. input & output
                        tol,                &  !  10. input & output
                        vectors_array,      &  !  11. output
                        values_array)          !  12. output

! 8. writing to the hard disk the eigensystem

  if (type_of_format_for_output == "unformatted") then

    ! 8.a. eigenvalues area. unformatted output

    open(unit=UNIT_RW,                   &
         file=file_for_eigenvalues,      &
         status="new",                   &
         action="write",                 &
         form=type_of_format_for_output)

    do i1 = 1, nev

      write(UNIT_RW) values_array(i1,1)

    end do

    flush(UNIT_RW)

    close(UNIT_RW, status="keep")

    ! 8.b. eigenvectors area. unformatted output

    open(unit=UNIT_RW,                  &
         file=file_for_eigenvectors,    &
         status="new",                  &
         action="write",                &
         form=type_of_format_for_output)


    do i1 = 1, nev
      do i2 = 1, matrix_dimen

        write(UNIT_RW) vectors_array(i2,i1)

      end do
    end do

    flush(UNIT_RW)

    close(UNIT_RW, status="keep")

  else if (type_of_format_for_output == "formatted") then

    ! 8.c. eigenvalues area. formatted output

    open(unit=UNIT_RW,                  &
         file=file_for_eigenvalues,     &
         status="new",                  &
         action="write",                &
         form=type_of_format_for_output)

    do i1 = 1, nev

      write(UNIT_RW, fmt=*) values_array(i1,1)

    end do

    flush(UNIT_RW)

    close(UNIT_RW, status="keep")

    ! 8.d. eigenvectors area. formatted output

    open(unit=UNIT_RW,                  &
         file=file_for_eigenvectors,    &
         status="new",                  &
         action="write",                &
         form=type_of_format_for_output)

    do i1 = 1, nev
      do i2 = 1, matrix_dimen

        write(UNIT_RW, fmt=*) vectors_array(i2,i1)

      end do
    end do

    flush(UNIT_RW)

    close(UNIT_RW, status="keep")

  else 

    write(*,*) " error: type of format must be 'formatted' of 'unformatted'"
    stop   

  end if

! 9. deallocations

  deallocate(row_index_array)
  deallocate(column_index_array)
  deallocate(ham_elem_array)
  deallocate(values_array)
  deallocate(vectors_array)

end program driver_arp_coo_omp_ram

! FINI
