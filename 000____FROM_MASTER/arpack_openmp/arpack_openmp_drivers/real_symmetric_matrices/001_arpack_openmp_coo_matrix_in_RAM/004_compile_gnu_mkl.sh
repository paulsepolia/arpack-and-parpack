#!/bin/bash

# 1. compiling the code
 
  gfortran  -O3                        \
		    -std=f2008                 \
            -fopenmp                   \
		    -cpp                       \
            m_1_type_definitions.f90   \
            m_2_matrix_coo_ram.f90     \
            m_3_arp_coo_omp_ram.f90    \
            driver_arp_coo_omp_ram.f90 \
            /opt/arpack/lib_2015/libarpack_gnu_ubu_ext.a             \
            /opt/mkl/libs_pgg/libmkl_core_thread_lp64_gnu_2015_pgg.a \
	        -ldl                                                     \
            -o x_gnu_mkl

# 2. cleaning the *.mod produced files

  rm *.mod

# 3. exiting
